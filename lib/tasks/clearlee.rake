namespace :clearlee do
  desc "Clearlee rake tasks..."
  task get_camera_data: :environment do
    c = CamerasCrawler.new
    urls = c.get_urls ("http://www.dpreview.com/products/cameras/all")
    c.process(urls) 
    c.populate_group
    Camera.where(:group => nil).delete_all
    c.update_dxomark_ratings
    c.populate_image_urls
    c.set_default_images
    c.populate_sample_images
    c.populate_alternate_prices_and_urls
    #c.populate_dpreview_ratings
  end

  task get_lens_data: :environment do
  	c = LensCrawler.new
    c.process(c.get_urls ("http://www.dpreview.com/products/lenses/all")) 
  end

  task populate_top_cameras: :environment do
    TopCameraData.delete_all
    camera_data = {}
    prices = [200, 500, 1000, 2000]
    
    prices.each do |p|
      data = Camera.top_cameras(5, "price", p)
      Rails.logger.debug data.inspect
      camera_data[["price",p]] = data
    end

    Camera.groups.each do |p|
      data = Camera.top_cameras(5, "type", p)
      camera_data[["type",p]] = data
    end

    camera_data.each do |k,v|
      TopCameraData.create(:criterion => k.first, :value => k.last, :data => Hash[*v[1].flatten], :total_cameras => v[0])
    end
  end

  task populate_engadget_score: :environment do
    CamerasCrawler.new.populate_engadget_score
  end

end
