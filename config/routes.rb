Sombrero::Application.routes.draw do
  resources :polls do
    member do
      put :vote
    end
  end
  resource :votes

  get 'home', to: 'cameras#splash', as: 'index'
  root 'cameras#splash'

  get 'inside', to: 'pages#inside', as: 'inside'
  
  resource :search do 
    get :solr
  end

  resources :cameras do
    collection do
      get :compare
      get :splash
      get :top_cameras
      post :compare_cameras
    end
    member do
      get :sample_images
    end
  end
  
  devise_for :users, :controllers => { :omniauth_callbacks => 'omniauth_callbacks'}
  
  namespace :admin do
    root 'base#index'
    resources :users
  end  

   get '*a', :to => 'errors#routing'
end
