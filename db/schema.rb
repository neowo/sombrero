# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141102145739) do

  create_table "cameras", force: true do |t|
    t.string   "brand",                      limit: 32
    t.string   "model_name"
    t.string   "mpn"
    t.string   "upc_code",                   limit: 32
    t.string   "asin",                       limit: 32
    t.string   "amzn_url",                   limit: 512
    t.string   "amzn_img_url",               limit: 512
    t.float    "amzn_price"
    t.date     "announced_date"
    t.string   "price_range",                limit: 32
    t.text     "executive_summary"
    t.string   "ae_bracketing",              limit: 256
    t.string   "aperture_priority"
    t.string   "articulated_lcd"
    t.string   "autofocus",                  limit: 512
    t.integer  "battery_life_cipa"
    t.string   "battery_description",        limit: 128
    t.string   "battery",                    limit: 512
    t.string   "body_type",                  limit: 64
    t.string   "built_in_flash",             limit: 128
    t.string   "continuous_drive",           limit: 128
    t.float    "fps"
    t.string   "custom_white_balance",       limit: 512
    t.string   "digital_zoom"
    t.integer  "digital_zoom_x"
    t.string   "dimensions",                 limit: 128
    t.float    "effective_megapixels"
    t.string   "environmentally_sealed",     limit: 128
    t.string   "exposure_compensation",      limit: 128
    t.string   "external_flash",             limit: 128
    t.string   "flash_modes",                limit: 512
    t.float    "flash_range_mts"
    t.string   "focal_length_equiv"
    t.integer  "focal_length_min"
    t.integer  "focal_length_max"
    t.float    "focal_length_multiplier"
    t.string   "format",                     limit: 128
    t.string   "gps_notes",                  limit: 512
    t.string   "gps",                        limit: 128
    t.string   "hdmi"
    t.string   "iso"
    t.integer  "iso_min"
    t.integer  "iso_max"
    t.string   "image_ratio_w_h"
    t.string   "image_stabilization_notes",  limit: 128
    t.string   "image_stabilization"
    t.string   "jpeg_quality_levels",        limit: 256
    t.string   "lens_mount",                 limit: 128
    t.string   "live_view",                  limit: 128
    t.integer  "macro_focus_range_cm"
    t.string   "manual_exposure_mode"
    t.string   "manual_focus"
    t.string   "max_resolution"
    t.string   "maximum_aperture"
    t.float    "max_aperture_zoomout"
    t.float    "max_aperture_zoomin"
    t.string   "maximum_shutter_speed"
    t.float    "max_shutter"
    t.string   "metering_modes",             limit: 256
    t.string   "microphone"
    t.string   "minimum_shutter_speed"
    t.float    "min_shutter"
    t.integer  "normal_focus_range_cms"
    t.string   "number_of_focus_points",     limit: 32
    t.integer  "optical_zoom"
    t.string   "orientation_sensor"
    t.text     "other_resolutions"
    t.string   "processor",                  limit: 128
    t.string   "remote_control"
    t.string   "video_resolutions",          limit: 256
    t.string   "full_hd"
    t.float    "full_hd_fps"
    t.string   "screen_dots"
    t.float    "screen_size"
    t.string   "screen_type",                limit: 128
    t.string   "self_timer",                 limit: 128
    t.string   "sensor_photo_detectors"
    t.string   "sensor_size",                limit: 128
    t.string   "sensor_size_type",           limit: 128
    t.float    "sensor_size_w_mm"
    t.float    "sensor_size_h_mm"
    t.string   "sensor_type",                limit: 128
    t.string   "shutter_priority"
    t.string   "speaker",                    limit: 128
    t.string   "storage_included",           limit: 256
    t.string   "storage_types",              limit: 256
    t.string   "subject_scene_modes",        limit: 512
    t.string   "timelapse_recording",        limit: 128
    t.string   "touch_screen"
    t.string   "usb"
    t.string   "uncompressed_format",        limit: 32
    t.text     "videography_notes"
    t.string   "viewfinder_coverage"
    t.float    "viewfinder_magnification"
    t.string   "viewfinder_type",            limit: 128
    t.string   "wb_bracketing",              limit: 256
    t.integer  "weight_inc_batteries_gms"
    t.integer  "white_balance_presets"
    t.string   "wireless",                   limit: 512
    t.datetime "created_at"
    t.float    "rating"
    t.boolean  "discontinued"
    t.datetime "updated_at"
    t.decimal  "dxo_rating",                             precision: 8, scale: 2
    t.text     "image_urls"
    t.string   "default_image_file_name"
    t.string   "default_image_content_type"
    t.integer  "default_image_file_size"
    t.datetime "default_image_updated_at"
    t.text     "sample_images"
    t.text     "timeline_info"
    t.string   "group"
    t.text     "alternate_urls_and_prices"
    t.string   "dpreview_rating"
    t.string   "engadget_score"
    t.string   "dpreview_url"
    t.string   "slug"
  end

  add_index "cameras", ["amzn_price", "group"], name: "ix_cameras_amzn_price_group", using: :btree
  add_index "cameras", ["group"], name: "ix_cameras_group", using: :btree
  add_index "cameras", ["model_name", "brand", "amzn_price"], name: "ix_cameras_model_name_brand_amzn_price", using: :btree
  add_index "cameras", ["upc_code"], name: "ix_cameras_upc_code", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "lens", force: true do |t|
    t.string   "brand"
    t.string   "model_name"
    t.text     "description"
    t.string   "mpn"
    t.string   "upc_code"
    t.string   "asin"
    t.string   "amzn_url"
    t.string   "amzn_img_url"
    t.float    "amzn_price"
    t.date     "announce_date"
    t.string   "price_range"
    t.text     "executive_summary"
    t.string   "lens_type"
    t.string   "max_format_size"
    t.integer  "focal_length_min"
    t.integer  "focal_length_max"
    t.string   "image_stabilization"
    t.string   "lens_mount"
    t.float    "maximum_aperture_zoomout"
    t.float    "maximum_aperture_zoomin"
    t.float    "minimum_aperture_zoomout"
    t.float    "minimum_aperture_zoomin"
    t.float    "aperture_ring"
    t.integer  "diaphragm_blades"
    t.string   "aperture_notes"
    t.integer  "elements"
    t.integer  "groups"
    t.string   "special"
    t.string   "minimum_focus"
    t.string   "minimum_focus_inch"
    t.string   "autofocus"
    t.string   "motor_type"
    t.string   "full_time_manual"
    t.string   "focus_method"
    t.string   "distance_scale"
    t.string   "dof_scale"
    t.integer  "weight_gms"
    t.integer  "diameter_mm"
    t.integer  "length_mm"
    t.string   "materials"
    t.string   "sealing"
    t.string   "color"
    t.string   "zoom_method"
    t.integer  "filter_thread_mm"
    t.string   "filter_notes"
    t.string   "hood_supplied"
    t.string   "hood_product_code"
    t.string   "tripod_collar"
    t.string   "optional_accessories"
    t.string   "discontinued"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "dxo_rating",               precision: 8, scale: 2
  end

  add_index "lens", ["model_name"], name: "index_lens_on_model_name", unique: true, using: :btree

  create_table "polls", force: true do |t|
    t.string   "products"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "question"
    t.string   "status"
    t.text     "image_urls"
    t.text     "title"
  end

  add_index "polls", ["status"], name: "ix_polls_status", using: :btree
  add_index "polls", ["user_id"], name: "index_polls_on_user_id", using: :btree

  create_table "top_camera_data", force: true do |t|
    t.string   "criterion"
    t.string   "value"
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "total_cameras"
  end

  add_index "top_camera_data", ["criterion", "value"], name: "index_top_camera_data_on_criterion_and_value", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "username",               default: "",    null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.boolean  "admin",                  default: false, null: false
    t.boolean  "locked",                 default: false, null: false
    t.string   "slug"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "authentication_token"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "user_id"
    t.integer  "poll_id"
    t.string   "upc_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
