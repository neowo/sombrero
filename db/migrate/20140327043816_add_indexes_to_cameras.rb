class AddIndexesToCameras < ActiveRecord::Migration
  def change
  	add_index :cameras, [:amzn_price, :group], :name => "ix_cameras_amzn_price_group"
  	add_index :cameras, [:group], :name => "ix_cameras_group"
  end
end
