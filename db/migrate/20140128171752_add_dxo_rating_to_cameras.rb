class AddDxoRatingToCameras < ActiveRecord::Migration
  def change
  	add_column :cameras, :dxo_rating, :decimal, :precision => 8, :scale => 2
  end
end
