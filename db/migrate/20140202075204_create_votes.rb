class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
    	t.integer :user_id
    	t.integer :poll_id
    	t.string :upc_code
      
      t.timestamps
    end
  end
end
