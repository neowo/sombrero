class AddUpcCodeIndexToCameras < ActiveRecord::Migration
  def change
  	add_index :cameras, [:upc_code], :name => "ix_cameras_upc_code"
  end
end
