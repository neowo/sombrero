class AddStatusToPolls < ActiveRecord::Migration
  def change
  	add_column :polls, :status, :string
  	add_index :polls, :status, :name => "ix_polls_status" 
  end
end
