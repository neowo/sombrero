class AddTimelineInfoToCamera < ActiveRecord::Migration
  def change
  	add_column :cameras, :timeline_info, :text
  end
end
