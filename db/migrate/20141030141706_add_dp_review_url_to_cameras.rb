class AddDpReviewUrlToCameras < ActiveRecord::Migration
  def change
    add_column :cameras, :dpreview_url, :string
  end
end
