class CreatePolls < ActiveRecord::Migration
  def change
    create_table :polls do |t|
      t.string :products
      t.integer :user_id

      t.timestamps
    end
    add_index :polls, :user_id,             :unique => false
  end
end
