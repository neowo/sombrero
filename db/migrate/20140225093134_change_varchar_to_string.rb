class ChangeVarcharToString < ActiveRecord::Migration
  def change
  	change_column :cameras, :viewfinder_coverage, :string
  	change_column :cameras, :shutter_priority, :string
  	change_column :cameras, :sensor_photo_detectors, :string
  	change_column :cameras, :screen_dots, :string
  	change_column :cameras, :full_hd, :string
  	change_column :cameras, :orientation_sensor, :string
  	change_column :cameras, :minimum_shutter_speed, :string
  	change_column :cameras, :microphone, :string
  	change_column :cameras, :maximum_shutter_speed, :string
  	change_column :cameras, :maximum_aperture, :string
  	change_column :cameras, :max_resolution, :string
  	change_column :cameras, :manual_focus, :string
  	change_column :cameras, :manual_exposure_mode, :string
  	change_column :cameras, :image_stabilization, :string
  	change_column :cameras, :image_ratio_w_h, :string
  	change_column :cameras, :hdmi, :string
  	change_column :cameras, :focal_length_equiv, :string
  	change_column :cameras, :digital_zoom, :string
  	change_column :cameras, :articulated_lcd, :string
  	change_column :cameras, :aperture_priority, :string
  end
end
