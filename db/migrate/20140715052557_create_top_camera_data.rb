class CreateTopCameraData < ActiveRecord::Migration
  def change
    create_table :top_camera_data do |t|
    	t.string :criterion
    	t.string :value
    	t.text :data
    	t.timestamps
    end
    add_index :top_camera_data, [:criterion, :value], :unique => true
  end
end
