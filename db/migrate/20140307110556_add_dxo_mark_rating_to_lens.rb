class AddDxoMarkRatingToLens < ActiveRecord::Migration
  def change
  	add_column :lens, :dxo_rating, :decimal, :precision => 8, :scale => 2
  end
end
