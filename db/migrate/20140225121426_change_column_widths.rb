class ChangeColumnWidths < ActiveRecord::Migration
  def change
  	change_column :cameras, :usb, :string
  	change_column :cameras, :model_name, :string
  end
end
