class ChangeVarcharToStringColumns < ActiveRecord::Migration
  def change
  	change_column :cameras, :touch_screen, :string
  	change_column :cameras, :mpn, :string
  	change_column :cameras, :iso, :string
  	change_column :cameras, :other_resolutions, :text
  end
end
