class AddDefaultImageToCameras < ActiveRecord::Migration
  def self.up
    add_attachment :cameras, :default_image
  end

  def self.down
    remove_attachment :cameras, :default_image
  end
end
