class AddSearchIndextoCameras < ActiveRecord::Migration
  def change
  	add_index :cameras, [:model_name, :brand, :amzn_price], :name => "ix_cameras_model_name_brand_amzn_price"
  end
end
