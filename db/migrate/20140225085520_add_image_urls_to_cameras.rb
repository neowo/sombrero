class AddImageUrlsToCameras < ActiveRecord::Migration
  def change
  	add_column :cameras, :image_urls, :text
  end
end
