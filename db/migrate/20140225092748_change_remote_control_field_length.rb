class ChangeRemoteControlFieldLength < ActiveRecord::Migration
  def change
  	change_column :cameras, :remote_control, :string
  end
end
