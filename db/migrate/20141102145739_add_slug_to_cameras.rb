class AddSlugToCameras < ActiveRecord::Migration
  def change
    add_column :cameras, :slug, :string
  end
end
