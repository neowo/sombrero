class CreateCameras < ActiveRecord::Migration
  def change
    create_table :cameras do |t|
    	t.string   "brand",                     limit: 32
      t.string   "model_name",                limit: 64
      t.string   "mpn",                       limit: 32
      t.string   "upc_code",                  limit: 32
      t.string   "asin",                      limit: 32
      t.string   "amzn_url",                  limit: 512
      t.string   "amzn_img_url",              limit: 512
      t.float    "amzn_price",                limit: 8
      t.date     "announced_date"
      t.string   "price_range",               limit: 32
      t.text     "executive_summary"
      t.string   "ae_bracketing",             limit: 256
      t.string   "aperture_priority",         limit: 16
      t.string   "articulated_lcd",           limit: 16
      t.string   "autofocus",                 limit: 512
      t.integer  "battery_life_cipa"
      t.string   "battery_description",       limit: 128
      t.string   "battery",                   limit: 512
      t.string   "body_type",                 limit: 64
      t.string   "built_in_flash",            limit: 128
      t.string   "continuous_drive",          limit: 128
      t.float    "fps",                       limit: 6
      t.string   "custom_white_balance",      limit: 512
      t.string   "digital_zoom",              limit: 16
      t.integer  "digital_zoom_x"
      t.string   "dimensions",                limit: 128
      t.float    "effective_megapixels",      limit: 6
      t.string   "environmentally_sealed",    limit: 128
      t.string   "exposure_compensation",     limit: 128
      t.string   "external_flash",            limit: 128
      t.string   "flash_modes",               limit: 512
      t.float    "flash_range_mts",           limit: 6
      t.string   "focal_length_equiv",        limit: 16
      t.integer  "focal_length_min"
      t.integer  "focal_length_max"
      t.float    "focal_length_multiplier",   limit: 6
      t.string   "format",                    limit: 128
      t.string   "gps_notes",                 limit: 512
      t.string   "gps",                       limit: 128
      t.string   "hdmi",                      limit: 16
      t.string   "iso",                       limit: 128
      t.integer  "iso_min"
      t.integer  "iso_max"
      t.string   "image_ratio_w_h",           limit: 16
      t.string   "image_stabilization_notes", limit: 128
      t.string   "image_stabilization",       limit: 16
      t.string   "jpeg_quality_levels",       limit: 256
      t.string   "lens_mount",                limit: 128
      t.string   "live_view",                 limit: 128
      t.integer  "macro_focus_range_cm"
      t.string   "manual_exposure_mode",      limit: 16
      t.string   "manual_focus",              limit: 16
      t.string   "max_resolution",            limit: 16
      t.string   "maximum_aperture",          limit: 16
      t.float    "max_aperture_zoomout",      limit: 6
      t.float    "max_aperture_zoomin",       limit: 6
      t.string   "maximum_shutter_speed",     limit: 16
      t.float    "max_shutter",               limit: 6
      t.string   "metering_modes",            limit: 256
      t.string   "microphone",                limit: 16
      t.string   "minimum_shutter_speed",     limit: 16
      t.float    "min_shutter",               limit: 8
      t.integer  "normal_focus_range_cms"
      t.string   "number_of_focus_points",    limit: 32
      t.integer  "optical_zoom"
      t.string   "orientation_sensor",        limit: 16
      t.string   "other_resolutions",         limit: 256
      t.string   "processor",                 limit: 128
      t.string   "remote_control",            limit: 16
      t.string   "video_resolutions",         limit: 256
      t.string   "full_hd",                   limit: 16
      t.float    "full_hd_fps",               limit: 6
      t.string   "screen_dots",               limit: 16
      t.float    "screen_size",               limit: 6
      t.string   "screen_type",               limit: 128
      t.string   "self_timer",                limit: 128
      t.string   "sensor_photo_detectors",    limit: 16
      t.string   "sensor_size",               limit: 128
      t.string   "sensor_size_type",          limit: 128
      t.float    "sensor_size_w_mm",          limit: 8
      t.float    "sensor_size_h_mm",          limit: 8
      t.string   "sensor_type",               limit: 128
      t.string   "shutter_priority",          limit: 16
      t.string   "speaker",                   limit: 128
      t.string   "storage_included",          limit: 256
      t.string   "storage_types",             limit: 256
      t.string   "subject_scene_modes",       limit: 512
      t.string   "timelapse_recording",       limit: 128
      t.string   "touch_screen",              limit: 128
      t.string   "usb",                       limit: 128
      t.string   "uncompressed_format",       limit: 32
      t.text     "videography_notes"
      t.string   "viewfinder_coverage",       limit: 16
      t.float    "viewfinder_magnification",  limit: 6
      t.string   "viewfinder_type",           limit: 128
      t.string   "wb_bracketing",             limit: 256
      t.integer  "weight_inc_batteries_gms"
      t.integer  "white_balance_presets"
      t.string   "wireless",                  limit: 512
      t.datetime "created_at"
      t.float    "rating",                    limit: 8
      t.boolean  "discontinued"
      t.timestamps
    end
  end
end
