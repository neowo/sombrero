json.array!(@votes) do |vote|
  json.extract! vote, :id, :upc_code, :user_id, :poll_id
  json.url vote_url(vote, format: :json)
end
