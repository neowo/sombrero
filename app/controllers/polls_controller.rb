class PollsController < ApplicationController
  before_action :set_poll, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, :all 

  # GET /polls
  # GET /polls.json
  def index
    # http://localhost:3000/polls.json?user_email=admin@example.com&user_token=myTSJyjBG2WzdWrMbGD3&my_polls=true&available_for_voting=true
    @polls = if params[:my_polls].present?
      current_user.polls
    else
      Poll.active
    end
    @polls -= current_user.polls_voted_for if params[:available_for_voting].present?
    return @polls
  end

  # GET /polls/1
  # GET /polls/1.json
  def show
    @poll = Poll.find(params[:id])
    if !@poll.viewable_by_user(current_user)
      render :status=>400,
      :json => { :message=>"You cannot view this poll." }
    else
      respond_to do |format|
        format.html
        format.json { render json: @poll }
      end
    end
  end

  # GET /polls/new
  def new
    @poll = current_user.polls.new
  end

  # GET /polls/1/edit
  def edit
    if !@poll.editable_by_user(current_user)
      render :status=>400,
      :json => { :message=>"You cannot edit this poll." }
      return
    end
  end

  # POST /polls
  # POST /polls.json
  def create
    # data : {"poll":{"question":"weqwe", "products":"qewqw", "user_id":"1"}}
    # http://localhost:3000/polls.json?user_email=admin@example.com&user_token=myTSJyjBG2WzdWrMbGD3
    received_params={}
    received_params["question"] = poll_params[:question]
    received_params["image_urls"] = poll_params[:image_urls]
    received_params["title"] = poll_params[:title]
    received_params["products"] = {}
    poll_params[:products].map { |x| received_params["products"][x]=0 }

    @poll = current_user.polls.new(received_params)
    
    respond_to do |format|
      if @poll.save
        format.html { redirect_to polls_url, notice: 'Poll was successfully created.'}
        format.json { render json: "Success".to_json, :status => 200 }
      else
        format.html { render action: 'new' }
        format.json { render json: @poll.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /polls/1
  # PATCH/PUT /polls/1.json
  def update
    respond_to do |format|
      if @poll.update(poll_params)
        format.html { redirect_to @poll, notice: 'Poll was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @poll.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /polls/1
  # DELETE /polls/1.json
  def destroy
    @poll.destroy
    respond_to do |format|
      format.html { redirect_to polls_url }
      format.json { head :no_content }
    end
  end

  def default_serializer_options
    {root: false}
  end

  # Update votecount
  def vote
    product_id,poll_id  = params[:UPC],params[:id]
    vote_count = Poll.update_vote_count(product_id,poll_id,current_user.id)
    if vote_count
      render :status => 200, :json => { :count => vote_count }
    else
      render :status => 422, :json => { :message=>"You have voted for this poll earlier." }
    end
  end
    
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poll
      @poll = Poll.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def poll_params
      params.require(:poll).permit(:user_id, :question, :image_urls => [], :title => [], :products => [])
    end

end
