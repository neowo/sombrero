# Responsible for returning the token on ligin http://localhost:3000/api/v1/tokens.json
class Api::V1::TokensController  < ApplicationController
  skip_before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token
  respond_to :json

  # Return the token on successful login
  #   params are email=email@domain.com&password=password
  def create
    email = params[:email]
    password = params[:password]
    if request.format != :json
      render :status => 406, :json => { :message=>"The request must be json" }
      return
    end

    if email.nil? or password.nil?
      render :status=>400,
      :json => { :message=>"The request must contain the user email and password." }
      return
    end

    @user = User.find_by_email(email.downcase)

    if @user.nil?
      logger.error("User #{email} failed signin, user cannot be found.")
      render :status => 401, :json => { :message=>"Invalid email or passoword." }
      return
    end

    if not @user.valid_password?(password)
      logger.error("User #{email} failed signin, password \"#{password}\" is invalid")
      render :status => 401, :json => { :message=>"Invalid email or password." }
    else
      render :status => 200, :json => { :token=>@user.authentication_token }
    end
  end

  # delete the token on signin
  def destroy
    @user = User.find_by_authentication_token(params[:id])
    if @user.nil?
      logger.error("Token not found for #{params.inspect}")
      render status: 404, :json => { :message => "Token not found" }
    else
      @user.authentication_token = ""
      @user.save!
      render :status => 200, :json => { :token => params[:id] }
    end
  end
end