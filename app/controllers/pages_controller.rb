# The pages Controller
class PagesController < ApplicationController
  # This line is to be added to each controller if you do not want it o be authenticated
  skip_before_filter :authenticate_user!
  
  before_action :authenticate_user!, only: [
    :inside
  ]

  # Home page
  def home
  end
  
  # A secure demo page visible only to admins
  def inside
  end 
    
end
