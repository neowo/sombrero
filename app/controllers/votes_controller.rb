class VotesController < ApplicationController
	skip_before_filter :verify_authenticity_token, :all 

	# Get all votes
  #
  # @return [Vote] votes
  def index
    # http://localhost:3000/votes.json?user_email=admin@example.com&user_token=myTSJyjBG2WzdWrMbGD3
		@votes = current_user.votes
	end

	# Get a particular vote
  #
  # @return [Vote]
	def show
		@vote = Vote.find(params[:id])
	end

	# Create a vote
  def create
    # http://localhost:3000/votes.json?user_email=admin@example.com&user_token=myTSJyjBG2WzdWrMbGD3
		@vote = current_user.votes.new(vote_params)

		respond_to do |format|
		  if @vote.save
		  	@vote.poll.vote(vote_params["upc_code"])
		    format.html { redirect_to @vote, notice: 'Vote was successfully created.' }
		    format.json { render action: 'show', status: :created, location: @vote }
		  else
		    format.html { render action: 'new' }
		    format.json { render json: @vote.errors, status: :unprocessable_entity }
		  end
		end
	end

	def new
		@vote = current_user.votes.new
	end

	private
  # Strict params checker
	def vote_params
      params.require(:vote).permit(:poll_id, :user_id, :upc_code)
   end
end
