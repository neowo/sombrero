class SearchesController < ApplicationController
  # This line is to be added to each controller if you do not want it o be authenticated
  skip_before_filter :authenticate_user!

  def solr
    if params[:term] == "" || params[:term].nil?
      redirect_to "/" and return
    end
    @search = Sunspot.search(Camera) do
      paginate :page => params[:page], :per_page => 24
      fulltext params[:term]
    end

    if @search.results == []
      @search = Sunspot.search(Camera) do |q|
        q.paginate :page => params[:page], :per_page => 24
        q.fuzzy(:model_name, params[:term])
      end
    end
    @products = @search.results
    @term = params[:term]
    @page = params[:page] || 1
    redirect_to "/" and return if @products.count == 0
    redirect_to camera_path(@products.first.id) and return if @products.count == 1
    respond_to do |solr|
      solr.html
      solr.json {
        render json: @products.to_json(only: [:model_name, :id], methods: [:default_image_url] )
      }
    end
  end
end
