class ErrorsController < ApplicationController
  def error_404
  	Rails.logger.debug "Rendering 404==========="
    @not_found_path = params[:not_found]
  end

  def error_500
  end

  def routing
  	redirect_to :error_404
  end
end