# The camera controller
class CamerasController < ApplicationController
  # This line is to be added to each controller if you do not want it o be authenticated
  skip_before_filter :authenticate_user!

  # List the cameras
  def index
    redirect_to(:action => :splash)
  end

  def splash 
    @price_brackets = TopCameraData.price_brackets
    @types = Camera.groups
  end

  # Show action
  def show
    @camera = Camera.friendly.find(params[:id])
    if request.path != camera_path(@camera)
      redirect_to @camera, status: :moved_permanently
    else
      #@review_url = AwsInterface.get_reviews(@camera.asin)

      @features = @camera.get_good_and_bad_in_price_bracket
      feature_percentage_ratings = {}
      @features.each do |k,v|
        next if v["total"] == 0
        h = {"better_percentage" => (v["better"].to_f * 100/v["total"].to_f).round(2),
             "worse_percentage" => (v["worse"].to_f * 100/v["total"].to_f).round(2)}
        feature_percentage_ratings[k] = h
      end
      good_array = feature_percentage_ratings.to_a.sort_by{|x,y|y["worse_percentage"]}.reverse[0..4]
      bad_array = feature_percentage_ratings.to_a.sort_by{|x,y|y["better_percentage"]}.reverse[0..4]
      @good_for_price = Hash[*good_array.flatten].keys
      @bad_for_price = Hash[*bad_array.flatten].keys

      @features_across_body_type = @camera.get_good_and_bad_in_price_bracket true
      feature_percentage_ratings_across_body_type = {}
      @features_across_body_type.each do |k,v|
        next if v["total"] == 0
        h = {"better_percentage" => (v["better"].to_f * 100/v["total"].to_f).round(2),
             "worse_percentage" => (v["worse"].to_f * 100/v["total"].to_f).round(2)}
        feature_percentage_ratings_across_body_type[k] = h
      end
      good_array = feature_percentage_ratings_across_body_type.to_a.sort_by{|x,y|y["worse_percentage"]}.reverse[0..4]
      bad_array = feature_percentage_ratings_across_body_type.to_a.sort_by{|x,y|y["better_percentage"]}.reverse[0..4]
      @good_for_price_across_body_type = Hash[*good_array.flatten].keys
      @bad_for_price_across_body_type = Hash[*bad_array.flatten].keys
      @similar_cameras_in_price_range = @camera.similar_products[:similar_products]
      @similar_cameras_in_brand = @camera.similar_products[:other_products_by_brand]
      @clearlee_rating = @camera.clearlee_rating.floor
      @clearlee_rating_across_body_type = @camera.clearlee_rating(true).floor
      @verdict = @camera.verdict @clearlee_rating, @clearlee_rating_across_body_type
      @verdict_in_group = @camera.verdict_in_group
      @price = @camera.amzn_price == 0.0 || @camera.amzn_price.nil? ? "NA" : @camera.amzn_price.to_s 
    end
  end
  
  def compare
    @cameras = Camera.all.map(&:model_name).reject{|c| c.nil?}
    @differences = nil
    if params[:model_name_one] && params[:model_name_two]
      @c1 = Camera.find_by_model_name(params[:model_name_one])
      @c2 = Camera.find_by_model_name(params[:model_name_two])
      @comparison_results = @c1.compare(@c2) if @c1 && @c2
    end
  end

  def compare_cameras
    similarities = [], out = []
    cameras =  Camera.find(params["selected_cameras"].split(","))
    fields = Camera.feature_weights.keys
    @common_features = common_features = {}
    fields.each do |f|
      val = ""
      common_features[f] = val if cameras.collect{|c| val = c.send(f)}.uniq.count == 1
    end
    @common_features = Camera.feature_descriptions.values_at(*common_features.keys).compact.uniq

    @good_and_bad = {}
    cameras.each do |c|
      @good_and_bad[c] = c.get_good_and_bad_in_price_bracket(false, cameras - [c])
    end
  end

  def sample_images
    @camera = Camera.find(params[:id])
  end

  def top_cameras
    tc = TopCameraData.where({criterion: params[:type], value: params[:value].tr('$', '')}).first
    cameras = Camera.find(tc.data.keys)
    data = []
    cameras.each do |c|
      data << {:id => c.id, :image_url => c.default_image.url, :clearlee_rating => tc.data[c.id], 
               :model_name => c.model_name, :price => c.amzn_price.nil? || c.amzn_price == 0.0 ? "NA" : c.amzn_price, :href => "/cameras/#{c.id}"}
    end
    render :json =>{:success => true, :data => data, :total_cameras => tc.total_cameras}
  end
end
