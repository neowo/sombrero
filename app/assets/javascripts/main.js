$(function() {
  	if($('.selectpicker')[0]) {
		$('.selectpicker').selectpicker();
	}
	$('.search-type a').click(function (e) {
		e.preventDefault();
		$(this).parent().addClass('active').siblings().removeClass('active');
	});
                     
	$('#best-price-btn, .best-price-btn').popover({
      html: true,
      trigger: 'click',
      content: function () {
          return $('#popover_content_wrapper').html();
      }
  });
	$("#best-price-btn, .best-price-btn").click(function(e){
		$('.best-price-btn').not(this).popover('hide');
          equalHeight($(".brand-icon"));
		$('#best-price-btn + .popover, .best-price-btn + .popover').width($('#popover_content_wrapper').children().length * 136).css({'right':164,'left':'auto'});
          e.preventDefault();
  });

	$('.compare-section table tr td, .compare-section table thead th + th').hover(function(){
		//$(this).addClass('shadow');	 
		var thisCol = $(this).index() + 1;
		console.log('.compare-section table td:nth-child('+thisCol+')');
		$('.compare-section table td:nth-child('+thisCol+'), .compare-section table thead th:nth-child('+thisCol+')').addClass('shadow');
	},function(){
		$('.compare-section table td,.compare-section table thead th').removeClass('shadow');
	});
	
	/*Auto adjust div heights*/
	equalHeight($("#pros-cons .col-md-10"));
	equalHeight($("#carousel-product-compare .product-desc"));
	equalHeight($(".spec-wrapper .eq-height"));
	equalHeight($(".rates-section > div"));
	equalHeight($(".product-sub .product-name"));
	equalHeight($('#tab-product-price > div'));
	equalHeight($('.compare-ht'));
	
	/* Make sections fit the screen */
	sectionHeight();
	$(window).resize(function(){
        sectionHeight();
    });	
});	

function sectionHeight(){
	$('.parallax section').css('min-height',$(window).height() - 50);
}

/* Equal height columns */
function equalHeight(group) {
   tallest = 0;
   group.each(function() {
	  thisHeight = $(this).height();
	  if(thisHeight > tallest) {
		 tallest = thisHeight;
	  }
   });
   group.height(tallest);
}

  // Cache selectors
  var lastId,
      topMenu = $("nav ul"),
      topMenuHeight = topMenu.outerHeight(),
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items for scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href=#"+id+"]").parent().addClass("active");
     }                   
  });


function sectionHeight(){
	$('.parallax section').css('min-height',$(window).height() - 50);
}

/* Equal height columns */
function equalHeight(group) {
   tallest = 0;
   group.each(function() {
	  thisHeight = $(this).height();
	  if(thisHeight > tallest) {
		 tallest = thisHeight;
	  }
   });
   group.height(tallest);
}