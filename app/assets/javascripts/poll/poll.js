// Increase voted counter on select of product and vote
$(function(){
  var clickedDiv;
  $(".poll-show-products").on('click', function() {
    clickedDiv = $(this);
    if($(".selected-to-vote")) {
      $(".selected-to-vote").removeClass("selected-to-vote");
    };
    clickedDiv.addClass('selected-to-vote');
  });

  $("#vote").on('click', function (e) {
    productCode = $(this).children().find("#product-code").attr("class");
    pollId = $("#poll-id").html();
    url = '/polls/'+pollId+'/vote';
    $.ajax({
      type: 'PUT',
      url: url,
      dataType: 'json',
      data: { "UPC" : productCode } ,
      success: function(data){
        clickedDiv.find(".vote-count").html(data["count"]);
      },
      error: function(xhr){
        alert("You have voted for this poll earlier.");
      }
    });
  });
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Create a new Poll
$(function(){
  var clickedDivPoll;
  $('body').on('click', '.poll-new-products-search-selected', function() {
    console.log($(this).html());
    clickedDivPoll.html($(this).html());
    $(".poll-show").removeClass('hidden');
    $('.search-to-vote').addClass('hidden');
  });

  // On click of add new product, go to search page
  $(".poll-new-products-click-to-add").on('click', function() {
    $(".poll-show").addClass('hidden');
    $('.search-to-vote').removeClass('hidden');
    clickedDivPoll = $(this);
    if($(".selected-to-poll")) {
      $(".selected-to-poll").removeClass("selected-to-poll");
    };
    clickedDivPoll.addClass('selected-to-poll');
  });
  
  // On Click of Search, get search results from BBYOpen
  $("#search-bby").on('click', function handleSearch(e) {
    var cBrand = $("#search-category").val();
    var search = $("#search-name").val();
    var bburl = "http://api.remix.bestbuy.com/v1/products%28manufacturer=" + cBrand + 
                "&productTemplate=Digital_Cameras&search=" + search + 
                "*%29?format=json&pageSize=10&show=name,sku,upc,thumbnailImage&apiKey=6gej7qzh2dtegu32hqds6zjv";
    $.ajax({
      type: "GET",
      async: false,
      url: bburl,     
      cache: true,
      dataType: "jsonp",
      jsonpCallback: 'jsonCallback',
      success: function(response){
        var output = '';
        for (var i = 0; i < response.products.length; i++) {
          var name = response.products[i].name;
          var sku = response.products[i].sku;
          var upc = response.products[i].upc;
          var thumbnailImage = response.products[i].thumbnailImage;
          output += '<div class="poll-new-products-search row">' + 
          '<a class="fill-div poll-new-products-search-selected" id="searchSelect" href="#">' + 
            '<div class="col-xs-2"><img src="' + thumbnailImage + '" class="product-image" id="' + upc + '"/></div>' +
            '<div class="col-xs-10 product-title">' + name + '</div>' +
            '</a>' + 
          '</div>';
        }
        $(".results").html(output);
      },
    });
   });

// {:products => []}, :user_id, :question, :image_urls => [], :title => []
  $("#create-poll").on('click', function (e) {
    var upcCode = [];
    var imageUrl = [];
    var productTitle = [];
    var question = $('.poll-new-question').find('#poll_question').val();
    for (var i = 0; i < $('.poll-new-products').length; i++) {
      upcCode.push($('.poll-new-products').find('.product-image')[i].id);
      imageUrl.push($('.poll-new-products').find('.product-image')[i].src);
      productTitle.push($('.poll-new-products').find('.product-title')[i].innerHTML);
    }
    console.log(upcCode);
    console.log(imageUrl);
    console.log(productTitle);
    console.log(question);


    $.ajax({
      type: 'POST',
      url: '/polls',
      dataType: 'json',
      data: { "poll" : {"question" : question, "image_urls" : imageUrl, "title" : productTitle, "products" : upcCode} } ,
      success: function(data){
        alert("done");
      },
      error: function(xhr){
        alert("You have voted for this poll earlier.");
      }
    });
  });
});