# The application helper
module ApplicationHelper
	# Specify the title in your views
  def title(value)
    unless value.nil?
      @title = "#{value} | Sombrero"      
    end
  end
end
