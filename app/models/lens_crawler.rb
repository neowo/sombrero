class LensCrawler < Crawler
  # Get the list of all url's to be crawled
  #
  # @param url [String]
  # @return [Array] shortlisted url's
  def get_urls url
    urls = super url
    urls.select{|u| u.match(/products(.*?)lenses/)}.reject{|u| u.match(/(ad\.|buy$|lenses\?)/)}.uniq
  end

  # method to process the url
  #
  # @param urls [String]
  def process urls
    urls.each do |url|
      begin 
        info = get_lens_info(url)
        create_or_update_lens(info)
      rescue => e
        puts e.message
      end
    end
  end

  def get_lens_info url
    puts "Processing #{url}"
		@asin, @model_name = get_asin_and_model_name url
		@brand = @model_name.split(" ").first
    return {} if @asin.nil?
    specs_page = open_page(url + "/specifications")
    specs_data_table = specs_page.css('tbody tr')
    @lens_type = get_spec_value specs_data_table, "Lens type"
    @max_format_size = get_spec_value specs_data_table, "Max Format size"
    focal_length = get_spec_value specs_data_table, "Focal length"
    if (@focal_length.present? && (focal_lengths = @focal_length.split.try(:first)) != nil)
      @focal_length_min, @focal_length_max = focal_lengths.split(/(\d+)/).reject{|s| !s.match(/^\d+$/)}
    end
    @image_stabilization = get_spec_value specs_data_table, "Image stabilisation"
    @lens_mount = get_spec_value specs_data_table, "Lens mount"

    maximum_aperture = get_spec_value specs_data_table, "Maximum aperture"
    if maximum_aperture.present?
      @maximum_aperture_zoomout = maximum_aperture.split('-').map(&:strip).first.match(/\d+\.*\d*/).to_s
      @maximum_aperture_zoomin = maximum_aperture.split('-').map(&:strip).last.match(/\d+\.*\d*/).to_s
    end

    minimum_aperture = get_spec_value specs_data_table, "Minimum aperture"
    if minimum_aperture.present?
      @minimum_aperture_zoomout = minimum_aperture.split('-').map(&:strip).first.match(/\d+\.*\d*/).to_s
      @minimum_aperture_zoomin = minimum_aperture.split('-').map(&:strip).last.match(/\d+\.*\d*/).to_s
    end
    @aperture_ring = get_spec_value specs_data_table, "Aperture ring"
    @aperture_notes = get_spec_value specs_data_table, "Aperture notes"
    @elements = get_spec_value specs_data_table, "Elements"
    @groups = get_spec_value specs_data_table, "Groups"
    @special = get_spec_value specs_data_table, "Special elements / coatings"
    minimum_focus = get_spec_value specs_data_table, "Minimum focus"
    @minimum_focus, @minimum_focus_inch = minimum_focus.scan(/\d+[.]\d+/) if minimum_focus
    @autofocus = get_spec_value specs_data_table, "Autofocus"
    @motor_type = get_spec_value specs_data_table, "Motor type"
    @full_time_manual = get_spec_value specs_data_table, "Full time manual"
    @focus_method = get_spec_value specs_data_table, "Focus method"
    @distance_scale = get_spec_value specs_data_table, "Distance scale"
    @dof_scale = get_spec_value specs_data_table, "DoF scale"
    wt = get_spec_value(specs_data_table, "Weight")
    @weight_gms = wt.scan(/\d+/).try(:first) if wt
    dia = get_spec_value(specs_data_table, "Diameter")
    @diameter_mm = dia.scan(/\d+/).try(:first) if dia
    len = get_spec_value(specs_data_table, "Length")
    @length_mm = len.scan(/\d+/).try(:first) if len
    @materials = get_spec_value(specs_data_table, "Materials")
    @sealing = get_spec_value(specs_data_table, "Sealing")
    @color = get_spec_value(specs_data_table, "Colour")
    @zoom_method = get_spec_value(specs_data_table, "Zoom method")
    ft = get_spec_value(specs_data_table, "Filter thread")
    @filter_thread_mm = ft.scan(/\d+/).try(:first) if ft
    @filter_notes = get_spec_value(specs_data_table, "Filter notes")
    @hood_supplied = get_spec_value(specs_data_table, 'Hood supplied')
    @hood_product_code  = get_spec_value(specs_data_table, 'Hood product code')
    @tripod_collar = get_spec_value(specs_data_table, 'Tripod collar')
    @optional_accessories = get_spec_value(specs_data_table, 'Optional accessories')

    h = {}
    instance_variables.each {|var| h[var.to_s.tr("@", "")] = instance_variable_get var}
		return h
  end

  def create_or_update_lens lens_info
    return if lens_info == {}
    asin = lens_info["asin"]
    aws_data = AwsInterface.get_data(lens_info["asin"], "Large,OfferFull")
    return if aws_data.nil?
    attributes = aws_data["Item"].first
   	
   	lens = Lens.find_or_create_by(:asin => asin)
    #lens.brand = attributes["ItemAttributes"]["Brand"]
    lens.upc_code = attributes["ItemAttributes"]["UPC"]
    lens.mpn = attributes["ItemAttributes"]["MPN"]
    lens.announce_date = attributes["ItemAttributes"]["ReleaseDate"]
    lens.amzn_url = attributes["DetailPageURL"]
    lens.amzn_img_url = attributes["LargeImage"]["URL"]

    if attributes["OfferSummary"].present? && attributes["OfferSummary"]["LowestNewPrice"].present? && attributes["OfferSummary"]["LowestNewPrice"]["FormattedPrice"].present?
      lens.amzn_price = attributes["OfferSummary"]["LowestNewPrice"]["FormattedPrice"].tr('$,', '').to_f
    end
    
    if attributes["EditorialReviews"].present? && attributes["EditorialReviews"]["EditorialReview"].present? 
      if attributes["EditorialReviews"]["EditorialReview"].kind_of?(Array)
        lens.executive_summary = attributes["EditorialReviews"]["EditorialReview"].first["Content"] if attributes["EditorialReviews"]["EditorialReview"].first.present?
      else
        lens.executive_summary = attributes["EditorialReviews"]["EditorialReview"]["Content"] if attributes["EditorialReviews"]["EditorialReview"]["Content"].present?
      end
    end

    lens_info.each{|k,v| lens.send("#{k}=", v)}
    lens.save!
  end


  def update_dxomark_ratings
    dxomark_url = "http://www.dxomark.com/dakdata/perfbody.php?resultSensorURL=http://www.dxomark.com/Lenses&sensormpix[]=1&sensormpix[]=300&sensorprice[]=0&sensorprice[]=50000&rp=28&viewtype=browse"
    page =  RestClient.post(dxomark_url, {'page'=>1, 'rp'=> 1000})
    
    json =  JSON.parse(page.body)
    json["rows"].each do |row|
      npage = Nokogiri::HTML(row["cell"].join(" "))
      lens_name = npage.css("a").select{|a| a.text if !a.text.blank? && !a.text.nil?}.first.text.split
      query = "brand like \"%#{lens_name.first}%\" " 
      lens_name[1..lens_name.length-1].each do |w|
        query += "and model_name like \"%#{w}%\" "
      end
      l = Lens.where("#{query}")
      if l.present?
        lens = l.first
        lens.dxo_rating = npage.css(".graphbar3").text.slice(0,2).to_f
        lens.save!
      end
    end
  end
end