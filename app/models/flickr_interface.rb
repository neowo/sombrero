require 'flickraw'

class FlickrInterface
	FlickRaw.api_key = "6123038eed0e47211235fe3f3f298cac"
	FlickRaw.shared_secret = "91c44fbcd75643d2"


	def self.sample_images model_name
		urls = []
		begin
			list = flickr.photos.search :tags => model_name, :per_page => 10, :page => 1
			list.each do |p|
			
				info = flickr.photos.getSizes :photo_id => p.id, :secret => p.secret
				
				image_list = []
				info.each {|i| image_list << i["source"]}
				urls << image_list
			end
		rescue => e
				Rails.logger.error "Error fetching images for camera #{model_name}: #{e.backtrace}"	
		end
		return urls
	end

	#urls << ["http://farm3.staticflickr.com/2276/" + p.id + "_" + p.secret + "_z.jpg",
			#		"http://farm3.staticflickr.com/2276/" + p.id + "_" + p.secret + "_c.jpg"] 
end