# The camera crawler model
class Crawler
	require 'mechanize'
	# Get the list of all url's to be crawled
  #
  # @param url [String]
  # @return [Array] aray of n urls to be crawled
	def get_urls url
		links = []
		agent = Mechanize.new
		page = agent.get(url)		

		page.links_with(:href => /^https?/).each do |link|
		  links << link.href
		end
		links
	end

	# Opens the page for the given url
  #
  # @param url [String]
  # @return [HTML] Nokogiri::HTML
	def open_page url
		Nokogiri::HTML(open(url))
	end

	def get_asin_and_model_name url
    page = open_page(url)
    asin_url = nil
    asin_link = page.css('tr').select{|link| link['data-link'].present? }
    asin_url = asin_link.first['data-link'] if asin_link && asin_link.first.present?
    asin_url = page.css('a').map {|element| element["href"]}.compact.select{|a| a.include? "ASIN"}.first if !asin_url 
    asin = nil
    model_name = nil
    asin = asin_url.split("%26").select{|s| s.match("creativeASIN")}.first.split("%3D").last if asin_url.present?
    asin = get_asin(url) if asin.nil?
    model_name = page.css('h1').first.text
    rating = page.css("td.numericalScore > div.score").try(:children).try(:first)
    [asin, model_name, rating]
	end

	def get_spec_value specs_data_table, spec
    text = specs_data_table.select{|tr| tr.text.match(Regexp.escape(spec))}.first.try(:text)
    return nil if text.nil?
    result = text.split.join(" ") 
    result.slice! spec if !result.nil?
    return result.strip.chomp("?").strip
  end

  def get_asin url
    system("wget -O /tmp/1 #{url}")
    str = IO.read('/tmp/1')
puts "String: #{str.index('creativeASIN')}"
    return nil if str.index('creativeASIN').nil?
puts "Index #{str.index('creativeASIN')}"
    start = str.index('creativeASIN') + 'creativeASIN'.size + 1
    ret = str[start..start+10]
puts "Returing #{ret}"
    #system("rm -f /tmp/1")
    ret
  end
end
