 #http://api.remix.bestbuy.com/v1/products%28manufacturer%20in%28canon,sony,nikon%29&search=coolpix&categoryPath.id=abcat0400000%29?apiKey=6gej7qzh2dtegu32hqds6zjv
# The BestBuyInterface 
class BestBuyInterface
  include HTTParty
  base_uri 'api.remix.bestbuy.com'
  default_params :apiKey => '6gej7qzh2dtegu32hqds6zjv'
 
  # get_all_stores
  def self.get_all_stores
    get("/v1/stores")
  end
 
  # get_stores_by_zip
  def self.get_stores_by_zip(zip)
    get("/v1/stores(postalCode=#{zip})")
  end
 
  # get_stores_by_zip_and_distance
  def self.get_stores_by_zip_and_distance(zip, distance)
    get("/v1/stores(area(#{zip},#{distance}))")
  end
 
  # get_product_by_sku
  def self.get_product_by_sku(sku)
    get("/v1/products/#{sku}.xml")
  end
 
  # get_products based on fiter
  def self.get_products(filter)
    response = get(URI.escape("/v1/products(#{filter})"))
    return response
     if response["products"] && response["products"]["product"]
      return response["products"]["product"]
    else
      return nil
    end
  end

  # get_product_images for the given upc
  def self.get_product_images(upc)
    products = self.get_products("UPC=#{upc}")
    products.select { |key, value| key.to_s.match(/Image/)} if products
  end

  #get price and url for the given upc
  def self.get_url_and_price(upc)
    begin 
      prod = BestBuyInterface.get_products("UPC=#{upc}")
    rescue => e
      return {}
    end
    return {} if prod.nil? 
      {"source" => "BestBuy", "url" => prod["url"], "price" => prod["salePrice"].present? ? prod["salePrice"] : prod["regularPrice"]}
  end

  # Every other thing
  def self.method_missing(method_id, *args)
    if match = /get_products_by_([_a-zA-Z]\w*)/.match(method_id.to_s)
      attribute_names = match.captures.last.split('_and_')
 
      request = ""
      attribute_names.each_with_index { |name, idx| request = request + name + "=" + args[idx] + (attribute_names.length-1 == idx ? "" : "&") }
 
      get_products(request)
    else
      super
    end
  end

  def self.get_reviews 
    filter = "sku=1780275"
    response = get(URI.escape("/v1/reviews(#{filter})"))
  end
end