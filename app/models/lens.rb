class Lens < ActiveRecord::Base
	include ProductHelper
	self.table_name = 'lens'
  searchable :auto_index => true, :auto_remove => true do
    text :model_name, :boost => 5.0
    text :brand
    text :upc_code
    integer :amzn_price
  end
end