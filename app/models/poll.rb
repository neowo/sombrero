# The model for polls
class Poll < ActiveRecord::Base
  belongs_to :user
  serialize :products, JSON
  serialize :image_urls, Array
  serialize :title, Array
  has_many :votes

  scope :active, -> { where(status: "active") }
  before_create {|p| p.status = 'active'}

  def editable_by_user user
  	user.admin || (self.status == 'active' && self.user == user)
  end

  def viewable_by_user user
  	user.admin || self.status == 'active'
  end

  def self.update_vote_count(upc_code, poll_id, user_id)
    if Vote.where(poll_id: poll_id, user_id: user_id).count == 0
      p = Poll.find poll_id
      p.products[upc_code] += 1
      p.save!
      Vote.create(poll_id: poll_id, upc_code: upc_code, user_id: user_id)
      p.products[upc_code]
    end
  end
end
