class TopCameraData < ActiveRecord::Base
	self.table_name = 'top_camera_data'
	serialize :data

	def self.price_brackets
		@price_brackets ||= TopCameraData.select{|c| c.criterion == 'price'}.map(&:value).uniq
	end
end