#Model to crawl cameras
class CamerasCrawler < Crawler
  include CameraHelper
  # Get the list of all url's to be crawled
  #
  # @param url [String]
  # @return [Array] shortlisted url's
  def get_urls url
    urls = super url
    return urls.select{|u| u.match(/(compacts|slrs)/)}.reject{|u| u.match(/buy$/)}.uniq
  end

  # method to process the url
  #
  # @param urls [String]
  def process urls
    urls.each do |url|
      begin 
        info = get_camera_info(url)
        create_or_update_camera(info, url)
      rescue => e
        puts e.message
      end
    end
  end

  # get camera info from the url
  #
  # @param url [String]
  # @return [Hash] info of each camera
  def get_camera_info url
    begin
      puts "Processing #{url}"
      asin, model_name, dpreview_rating = get_asin_and_model_name url
      puts "#{asin}:#{model_name}:#{dpreview_rating}"
      return {} if asin.nil?
      specs_page = open_page(url + "/specifications")
      specs_data_table = specs_page.css('tbody tr')
      battery = get_spec_value specs_data_table, "Battery"
      battery_life_cipa_text = specs_data_table.select{|tr| tr.text.match("CIPA")}
      battery_life_cipa = nil
      if battery_life_cipa_text.present?
        battery_life_cipa = specs_data_table.select{|tr| tr.text.match("CIPA")}.first.text.split.join(" ")
        battery_life_cipa.slice! "Battery Life (CIPA)"
      end
      battery_description = get_spec_value specs_data_table, "Battery description"
      body_type = get_spec_value specs_data_table, "Body type"
      built_in_flash = get_spec_value specs_data_table, "Built-in flash"
      continuous_drive = get_spec_value specs_data_table, "Continuous drive"
      effective_megapixels = get_spec_value specs_data_table, "Effective pixels"
      flash_range_mts = get_spec_value specs_data_table, "Flash range"
      focal_length_equiv = get_spec_value specs_data_table, "Focal length (equiv.)"
      if (focal_length_equiv.present? && (focal_lengths = focal_length_equiv.split.try(:first)) != nil)
        focal_length_min, focal_length_max = focal_lengths.split(/(\d+)/).reject{|s| !s.match(/^\d+$/)}
      end

      focal_length_multiplier = get_spec_value specs_data_table, "Focal length multiplier"
      iso = get_spec_value specs_data_table, "ISO"
      if iso.present?
        iso_min = iso.split(',').map(&:strip).select{|i| i.match(/\d+/)}.try(:first)
        iso_max = iso.split(',').map(&:strip).select{|i| i.match(/\d+/)}.try(:last)
      end
      #TODO: get fps, shutter, min shutter, max shutter, full_hd, full hd fps
      macro_focus_range_cm = get_spec_value specs_data_table, "Macro focus range"
      maximum_aperture = get_spec_value specs_data_table, "Maximum aperture"

      if maximum_aperture.present?
        max_aperture_zoomout = maximum_aperture.split('-').map(&:strip).first.match(/\d+\.*\d*/).to_s
        max_aperture_zoomin = maximum_aperture.split('-').map(&:strip).last.match(/\d+\.*\d*/).to_s
      end

      normal_focus_range_cms = get_spec_value specs_data_table, "Normal focus range"
      normal_focus_range_cms = get_spec_value specs_data_table, "Normal focus range"
      optical_zoom = get_spec_value specs_data_table, "Optical zoom"
      optical_zoom = optical_zoom.match(/\d+/).to_s if optical_zoom.present?
      video_resolutions = get_spec_value specs_data_table, "Resolutions"
      sensor_size = get_spec_value specs_data_table, "Sensor size"
      if sensor_size.present?
        strs = sensor_size.tr(')(','').split
        sensor_type = strs[0]
        sensor_size_w_mm, sensor_size_h_mm = strs.select{|s| s.match(/\d+\.*\d*/)}
      end

      sensor_type = get_spec_value specs_data_table, "Sensor type"
      weight_inc_batteries_gms = nil
      weight_text = get_spec_value(specs_data_table, "Weight (inc. batteries)")
      if weight_text.present?
        weight_inc_batteries_gms = weight_text.split(" ").try(:first)
      end

      ae_bracketing = get_spec_value(specs_data_table, "AE Bracketing")
      aperture_priority = get_spec_value(specs_data_table, "Aperture priority")
      articulated_lcd = get_spec_value(specs_data_table, "Articulated LCD")
      auto_focus = get_spec_value(specs_data_table, "Autofocus")
      custom_white_balance = get_spec_value(specs_data_table, "Custom white balance")
      digital_zoom = get_spec_value(specs_data_table, "Digital zoom")
      environmentally_sealed = get_spec_value(specs_data_table, "Environmentally sealed")
      exposure_compensation =  get_spec_value(specs_data_table, "Exposure compensation")
      external_flash = get_spec_value(specs_data_table, "External flash")
      flash_modes = get_spec_value(specs_data_table, "Flash modes")
      format = get_spec_value(specs_data_table, "Format")
      gps_notes = get_spec_value(specs_data_table, "GPS notes")
      hdmi = get_spec_value(specs_data_table, "HDMI")
      image_ratio_w_h = get_spec_value(specs_data_table, "Image ratio w:h")
      image_stabilization_notes = get_spec_value(specs_data_table, "Image stabilization notes")
      image_stabilization = get_spec_value(specs_data_table, "Image stabilization")
      jpeg_quality_levels = get_spec_value(specs_data_table, "JPEG quality levels")
      lens_mount = get_spec_value(specs_data_table, "Lens mount")
      live_view = get_spec_value(specs_data_table, "Live view")
      manual_exposure_mode = get_spec_value(specs_data_table, "Manual exposure mode")
      manual_focus = get_spec_value(specs_data_table, "Manual focus")
      max_resolution = get_spec_value(specs_data_table, "Max resolution")
      metering_modes = get_spec_value(specs_data_table, "Metering modes")
      microphone = get_spec_value(specs_data_table, "Microphone")
      number_of_focus_points = get_spec_value(specs_data_table, "Number of focus points")
      orientation_sensor = get_spec_value(specs_data_table, "Orientation sensor")
      other_resolutions = get_spec_value(specs_data_table, "Other resolutions")
      processor = get_spec_value(specs_data_table, "Processor")
      remote_control = get_spec_value(specs_data_table, "Remote control")
      screen_dots = get_spec_value(specs_data_table, "Screen dots")
      screen_size = get_spec_value(specs_data_table, "Screen size")
      screen_type = get_spec_value(specs_data_table, "Screen type")
      self_timer = get_spec_value(specs_data_table, "Self-timer")
      sensor_photo_detectors = get_spec_value(specs_data_table, "Sensor photo detectors")
      shutter_priority = get_spec_value(specs_data_table, "Shutter priority")
      speaker = get_spec_value(specs_data_table, "Speaker")
      storage_included = get_spec_value(specs_data_table, "Storage included")
      storage_types = get_spec_value(specs_data_table, "Storage types")
      subject_scene_modes = get_spec_value(specs_data_table, "Subject / scene modes")
      timelapse_recording  = get_spec_value(specs_data_table, "Timelapse recording")
      touch_screen = get_spec_value(specs_data_table, "Touch screen")
      usb = get_spec_value(specs_data_table, "USB")
      uncompressed_format = get_spec_value(specs_data_table, "Uncompressed format")
      videography_notes = get_spec_value(specs_data_table, "Videography notes")
      viewfinder_coverage =  get_spec_value(specs_data_table, "Viewfinder coverage")
      viewfinder_magnification = get_spec_value(specs_data_table, "Viewfinder magnification")
      viewfinder_type = get_spec_value(specs_data_table, "Viewfinder type")
      wb_bracketing = get_spec_value(specs_data_table, "WB Bracketing")
      white_balance_presets = get_spec_value(specs_data_table, "White balance presets")
      wireless = get_spec_value(specs_data_table, "Wireless")
      maximum_shutter_speed = get_spec_value(specs_data_table, "Maximum shutter speed")
      if maximum_shutter_speed.present? 
        speed = maximum_shutter_speed.split.try(:first)
        if speed.present? 
          maximum_shutter_speed = speed.split("/").try(:last)
        else
          maximum_shutter_speed = nil
        end 
      end
      minimum_shutter_speed = get_spec_value(specs_data_table, "Minimum shutter speed")
      if minimum_shutter_speed.present? 
        speed = maximum_shutter_speed.split.try(:first)
        if speed.present? 
          minimum_shutter_speed = 1/speed.to_f
        else
          minimum_shutter_speed = nil
        end 
      end

      announced_date = nil
      begin
        announced_date = specs_page.css('.shortSpecs').first.try(:inner_text).split("\r\n").map(&:strip).grep(/Announced/).first.split[1..-1].join(' ').to_datetime
        announced_date = specs_page.css('.shortSpecs').first.try(:text).split("\r\n").map(&:strip).grep(/Announced/).first.split[1..-1].join(' ').to_datetime if announced_date.nil?
        puts "announced : #{announced_date.inspect}"
      rescue => e
        Rails.logger.error "Error setting announced_date for #{model_name}"
      end

      h = {"asin" => asin, "model_name" => model_name, "battery_life_cipa" => battery_life_cipa,
      "battery_description" => battery_description, "battery" => battery, "body_type" => body_type,
      "built_in_flash" => built_in_flash, "continuous_drive" => continuous_drive,
      "effective_megapixels" => effective_megapixels, "flash_range_mts" => flash_range_mts,
      "focal_length_equiv" => focal_length_equiv, "focal_length_min" => focal_length_min,
      "focal_length_max" => focal_length_max, "focal_length_multiplier" => focal_length_multiplier,
      "iso" => iso, "iso_min" => iso_min, "iso_max" => iso_max, "macro_focus_range_cm" => macro_focus_range_cm,
      "maximum_aperture" => maximum_aperture, "max_aperture_zoomin" => max_aperture_zoomin,
      "max_aperture_zoomout" => max_aperture_zoomout, "normal_focus_range_cms" => normal_focus_range_cms,
      "optical_zoom" => optical_zoom, "video_resolutions" => video_resolutions, "sensor_size" => sensor_size,
      "sensor_type" => sensor_type, "sensor_size_w_mm" => sensor_size_w_mm, "sensor_size_h_mm" => sensor_size_h_mm,
      "weight_inc_batteries_gms" => weight_inc_batteries_gms, "ae_bracketing" => ae_bracketing, 
      "aperture_priority" => aperture_priority, "articulated_lcd" => articulated_lcd,
      "autofocus" => auto_focus, "custom_white_balance" => custom_white_balance, "digital_zoom" => digital_zoom,
      "environmentally_sealed" => environmentally_sealed, "exposure_compensation" => exposure_compensation,
      "external_flash" => external_flash, "flash_modes" => flash_modes, "format" => format, "gps_notes" => gps_notes,
      "hdmi" => hdmi, "image_ratio_w_h" => image_ratio_w_h, "image_stabilization_notes" => image_stabilization_notes,
      "image_stabilization" => image_stabilization, "jpeg_quality_levels" => jpeg_quality_levels,
      "lens_mount" => lens_mount, "live_view" => live_view, "manual_exposure_mode" => manual_exposure_mode,
      "manual_focus" => manual_focus, "max_resolution" => max_resolution, "metering_modes" => metering_modes,
      "microphone" => microphone, "number_of_focus_points" => number_of_focus_points,
      "orientation_sensor" => orientation_sensor, "other_resolutions" => other_resolutions, "processor" => processor,
      "remote_control" => remote_control, "screen_dots" => screen_dots, "screen_type" => screen_type,
      "screen_size" => screen_size, "self_timer" => "self_timer", "sensor_photo_detectors" => sensor_photo_detectors,
      "shutter_priority" => shutter_priority, "speaker" => speaker, "storage_included" => storage_included,
      "storage_types" => storage_types, "subject_scene_modes" => subject_scene_modes,
      "timelapse_recording" => timelapse_recording, "touch_screen" => touch_screen, "usb" => usb,
      "uncompressed_format" => uncompressed_format, "videography_notes" => videography_notes,
      "viewfinder_coverage" => viewfinder_coverage, "viewfinder_magnification" => viewfinder_magnification,
      "viewfinder_type" => viewfinder_type, "wb_bracketing" => wb_bracketing, "white_balance_presets" => white_balance_presets,
      "wireless" => wireless, "discontinued" => false, "maximum_shutter_speed" => maximum_shutter_speed, "fps" => continuous_drive,
      "minimum_shutter_speed" => minimum_shutter_speed, "announced_date" => announced_date, "dpreview_rating" => dpreview_rating
      }
    rescue => e
      puts e.message
    end
  end

  # create_or_update_camera in the db
  #
  # @param camera_info [Hash]
  def create_or_update_camera camera_info, url
puts "Update entry"
    return if camera_info == {} || camera_info.nil?
    asin = camera_info["asin"]
    camera = Camera.find_or_create_by(:asin => asin)
    camera.dpreview_url = url
    aws_data = AwsInterface.get_data(camera_info["asin"], "Large,OfferFull")
    return if aws_data.nil?
    attributes = aws_data["Item"].first
    if attributes
      camera.brand = attributes["ItemAttributes"]["Brand"]
      camera.upc_code = attributes["ItemAttributes"]["UPC"]
      camera.mpn = attributes["ItemAttributes"]["MPN"]
      dimensions = ""
      if attributes["ItemAttributes"]["ItemDimensions"].present?
        attributes["ItemAttributes"]["ItemDimensions"].each do |k,v|
          dimensions.concat(k.to_s).concat(":").concat(v["__content__"]).concat(",").chop
        end
      end
      camera.dimensions = dimensions
      if attributes["OfferSummary"].present? && attributes["OfferSummary"]["LowestNewPrice"].present? && attributes["OfferSummary"]["LowestNewPrice"]["FormattedPrice"].present?
        camera.amzn_price = attributes["OfferSummary"]["LowestNewPrice"]["FormattedPrice"].tr('$,', '').to_f
      elsif attributes["OfferSummary"].present? && attributes["OfferSummary"]["LowestUsedPrice"].present? && attributes["OfferSummary"]["LowestUsedPrice"]["FormattedPrice"].present?
        camera.amzn_price = attributes["OfferSummary"]["LowestUsedPrice"]["FormattedPrice"].tr('$,', '').to_f
      end
      
      if attributes["EditorialReviews"].present? && attributes["EditorialReviews"]["EditorialReview"].present? 
        if attributes["EditorialReviews"]["EditorialReview"].kind_of?(Array)
          camera.executive_summary = attributes["EditorialReviews"]["EditorialReview"].first["Content"] if attributes["EditorialReviews"]["EditorialReview"].first.present?
        else
          camera.executive_summary = attributes["EditorialReviews"]["EditorialReview"]["Content"] if attributes["EditorialReviews"]["EditorialReview"]["Content"].present?
        end
      end
      camera.amzn_url = attributes["DetailPageURL"]
      camera.amzn_img_url = attributes["LargeImage"]["URL"]
    end
    camera_info.each{|k,v| camera.send("#{k}=", v)}
    camera.save
    #d = Date.new
    #d = camera_info["announced_date"].strftime("%m/%d/%Y")
    #camera.announced_date = d #camera_info["announced_date"].to_date
    #camera.save
    puts "end update"
  end

  # update camera ratings from dxomark
  #
  def update_dxomark_ratings
    dxomark_url = "http://www.dxomark.com/dakdata/perfbody.php?resultSensorURL=http://www.dxomark.com/Cameras&sensormpix[]=1&sensormpix[]=300&sensorprice[]=0&sensorprice[]=50000&rp=28&viewtype=browse"
    page =  RestClient.post(dxomark_url, {'page'=>1, 'rp'=> 1000})
    
    json =  JSON.parse(page.body)
    json["rows"].each do |row|
      npage = Nokogiri::HTML(row["cell"].join(" "))
      camera_name = npage.css("a").select{|a| a.text if !a.text.blank? && !a.text.nil?}.first.text.split
      query = "brand like \"%#{camera_name.first}%\" " 
      camera_name[1..camera_name.length-1].each do |w|
        query += "and model_name like \"%#{w}%\" "
      end
      c = Camera.where("#{query}")
      if c.present?
        camera = c.first
        camera.dxo_rating = npage.css(".graphbar3").text.slice(0,2).to_f
        camera.save!
      end
    end
  end

  def populate_image_urls
    Camera.where("upc_code is not null and upc_code != ''").each do |c|
      c.image_urls = BestBuyInterface.get_product_images c.upc_code
      c.save! if c.image_urls
    end
  end

  def set_default_images
    Camera.where("amzn_img_url is not null").each do |c|
      c.set_image_from_url(c.amzn_img_url)
    end
  end

  def populate_sample_images
    Camera.where("model_name is not null").each do |c|
      c.sample_images = FlickrInterface.sample_images(c.model_name)
      c.save!
    end
  end

  def parse_timeline_info rows, model_name
    arr = []; rows.each do |r| ;arr << r.children.reject{|c| c.text == "\n"}.map{|c| [c.text.strip.tr("\n", "/"), c['colspan']]};end
    data_rows = arr[2..arr.length-1]
    full_data = []

    data_rows.each do |dr|
      out = []
      qtr = 0
      dr.each do |a|
        if a[0] == ""
          qtr += a[1].to_i
        else
          temp = qtr + a[1].to_i
          models = a[0].split("/").map(&:strip)
          models.each do |m|
            out << {id: get_camera_from_brand_and_model_name(model_name, m).try(:id), model: m, start_qtr: getQuarter(1999, qtr), end_qtr: getQuarter(1999, temp)}
          end
          qtr = temp
        end
      end
      full_data << out
    end
    
    full_data.each do |data_row|
      data_row.each_with_index do |c,i|
        c[:predecessor] = data_row[i-1][:id] if i-1 >= 0
        c[:successor] = data_row[i+1][:id] if i+1 <= data_row.length-1
      end
    end

    full_data.flatten.select{|d| d[:id].present?}.each do |d|
      c = Camera.find d[:id]
      c.timeline_info = d
      c.save!
    end
  end

  def populate_nikon_timeline_info
    page = open_page "http://en.wikipedia.org/wiki/Template:Nikon_DSLR_cameras"
    rows = page.css(".hlist tr")
    parse_timeline_info rows, "nikon"
  end

  def populate_canon_timeline_info
    page = open_page "http://en.wikipedia.org/wiki/Template:Canon_DSLR_cameras"
    rows = page.css(".wikitable tr")
    parse_timeline_info rows, "canon"
  end

  def populate_olympus_timeline_info
    page = open_page "http://en.wikipedia.org/wiki/Template:Olympus_DSLR_cameras"
    rows = page.css(".wikitable tr")
    parse_timeline_info rows, "olympus"
  end  

  def populate_pentax_timeline_info
    page = open_page "http://en.wikipedia.org/wiki/Template:Pentax_DSLR_cameras"
    rows = page.css(".wikitable tr")
    parse_timeline_info rows, "pentax"
  end  

  def populate_group
    Camera.where('body_type in ("Compact", "Ultracompact")').update_all(:group => "Compact")
    Camera.where('body_type like "%SLR-style mirrorless%"').update_all(:group => "DSLR style mirrorless")
    Camera.where('body_type like "SLR-like%" or body_type like "Large sensor compact%"').update_all(:group => "Semi pro bridge")
    Camera.where('body_type like "Rangefinder%"').update_all(:group => "Rangefinder")
    Camera.where('(body_type like "%Mid-size SLR%" or body_type like "%Compact SLR%" or body_type like "%Large SLR%")').update_all(:group => "Crop Sensor DSLR")
    Camera.where('(body_type like "%Mid-size SLR%" or body_type like "%Compact SLR%" or body_type like "%Large SLR%") and sensor_size like "%full frame%"').update_all(:group => "Full frame DSLR")
  end

  def populate_alternate_prices_and_urls
    Camera.where("upc_code is not null and upc_code != ''").each do |c|
      c.alternate_urls_and_prices = BestBuyInterface.get_url_and_price(c.upc_code)
      c.save
    end
  end

  def populate_engadget_score
    urls = Camera.all.reject{|c| c.model_name.nil?}.map{|c| [c, "http://www.engadget.com/products/" + c.full_name.strip.split(/\s+/).join("/")]}.uniq
    urls.each do |c, u|
      puts u
      begin
        page = open_page u
        #score = page.css('.engadget-global-score').try(:children).try(:first).try(:text).strip
        score = page.css('.flyout-score').try(:children).try(:first).try(:text).try(:strip)
        puts score
        c.engadget_score = score
        c.save!
      rescue => e
         puts e.inspect
      end
    end
  end

end
