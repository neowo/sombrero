# The camera model
#timeline
#Visual representation of ratings
# Curate reviews


class Camera < ActiveRecord::Base
include ProductHelper

serialize :image_urls
serialize :timeline_info
serialize :alternate_urls_and_prices
serialize :sample_images, Array
scope :with_price, -> {where("amzn_price <> ''")}

extend FriendlyId
friendly_id :model_name, use: [:slugged, :history]

has_attached_file :default_image, 
                  :styles => { :medium => "300x300>", :thumb => "100x100>" },
                  :path => ":rails_root/public/assets/:class/:id/:style/:filename",
                  :url => "/assets/:class/:id/:style/:filename",
                  :default_url => "/assets/:class/image_missing.png"
                  #:path => "#{Rails.root}/system/:class/:id/:style/:filename",
                  #:url => "/system/:class/:id/:style/:filename"
 # validates_attachment :default_image, :content_type => { :content_type => ["image/jpg", "image/gif", "image/png"] }
# Validate content type
  #validates_attachment_content_type :default_image, :content_type => /\Aimage\/.*\Z/
# Validate filename
  #validates_attachment_file_name :default_image, :matches => [/png\Z/, /jpe?g\Z/]
# Explicitly do not validate
  do_not_validate_attachment_file_type :default_image
  #do_not_validate_attachment_content_type :default_image
  self.primary_key = :id

  searchable :auto_index => true, :auto_remove => true do
    text :model_name, :boost => 5.0
    text :brand
    text :upc_code
    integer :amzn_price
    text :group
  end

  @@feature_weights = {
    "announced_date" => 0.5, #handle separately
    "ae_bracketing" => 0.1, #Need to convert this to yes/no
    "aperture_priority" => 1.0,
    "articulated_lcd" => 0.6,
    "battery_life_cipa" => 0.8,
    "fps" => 0.9,
    "effective_megapixels" => 1.0,
    "environmentally_sealed" => 0.7,
    "external_flash" => 0.5,
    "flash_range_mts" => 0.5,
    "focal_length_min" => 0.7, # This should be lowest
    "focal_length_max" => 0.9, # This should be highest
    "gps" => 0.1, 
    "hdmi" => 0.1,
    "iso_min" => 0.2,
    "iso_max" => 0.9,
    "image_stabilization" => 0.7,
    "live_view" => 0.8,
    "macro_focus_range_cm" => 0.7, #Should be lowest
    "manual_exposure_mode" => 0.7,
    "manual_focus" => 0.5,
    #"maximum_aperture" => 0.9, #TODO
    "max_aperture_zoomout" => 0.9,
    "max_aperture_zoomin" => 0.9,
    "maximum_shutter_speed" => 0.4,
    "minimum_shutter_speed" => 0.4,
    "number_of_focus_points" => 0.5,
    "optical_zoom" => 0.9,
    "full_hd_fps" => 0.8,
    "screen_size" => 0.6,
    "sensor_size_w_mm" => 1.0,
    "sensor_size_h_mm" => 1.0,
    "shutter_priority" => 1.0,
    "timelapse_recording" => 0.7
  }

  @@feature_descriptions = {
    "announced_date" => {"feature_code" => "announced_date",
                         "text" => "Announce Date",
                         "good" => "A fairly recent model",
                         "bad" => "Newer cameras are available"}, #handle separately
    "ae_bracketing" => { "feature_code" => "ae_bracketing" ,
                         "text" => "Auto Exposure Bracketing",
                         "good" => "Has Auto Exposure Bracketing",
                         "bad" => "Lacks Auto Exposure Bracketing"}, #Need to convert this to yes/no
    "aperture_priority" => {"feature_code" => "aperture_priority",
                          "text" => "Aperture and Shutter Priority",
                         "good" => "Allows semi automatic shooting modes",
                         "bad" => "Does not allow semi automatic shooting modes"},
    "articulated_lcd" => {"feature_code" => "articulated_lcd",
                         "text" => "Swiveling LCD display",
                         "good" => "Has swiveling LCD",
                         "bad" => "Lacks swiveling LCD"},
    "battery_life_cipa" => {"feature_code" => "battery_life_cipa",
                            "text" => "Battery Life",
                            "good" => "Good battery life",
                            "bad" => "Battery life could be better"},
    "fps" => {"feature_code" => "fps",
              "text" => "Number of continuous shots the camera can take",
              "good" => "Higher number of shots in continuous/burst mode",
              "bad" => "Number of shots in continuous/burst mode could be better"},
    "effective_megapixels" => {"feature_code" => "effective_megapixels",
                               "text" => "Resolution in terms of megapixels",
                               "good" => "Can take high resolution pictures",
                               "bad" => "Resolution capacity could be better"},
    "environmentally_sealed" => {"feature_code" => "environmentally_sealed",
                               "text" => "Weather proofing",
                               "good" => "Weather proofing present",
                               "bad" => "Lacks weather proofing"},
    "external_flash" => {"feature_code" => "external_flash",
                         "text" => "External Flash",
                         "good" => "Popup flash present",
                         "bad" => "Lacks popup flash"},
    "flash_range_mts" => {"feature_code" => "flash_range_mts",
                         "text" => "Flash Range",
                         "good" => "Good flash ",
                         "bad" => "Lacks popup flash"},
    "focal_length_min" => {"feature_code" => "focal_length_min",
                         "text" => "Ability to take wide pictures",
                         "good" => "Can take wide pictures",
                         "bad" => "Ability to take wide pictures could be better"},
    "focal_length_max" => {"feature_code" => "focal_length_max",
                         "text" => "Ability to zoom",
                         "good" => "Has good zooming capability",
                         "bad" => "Zoom could be better"},
    "gps" => {"feature_code" => "gps",
                        "text" => "GPS tagging",
                         "good" => "Has GPS tagging capability",
                         "bad" => "Lacks GPS tagging"}, 
    "hdmi" => {"feature_code" => "hdmi",
                         "text" => "HDMI Connectivity",
                         "good" => "Has HDMI Connectivity",
                         "bad" => "Lacks HDMI Connectivity"}, 
    "iso_min" => {"feature_code" => "iso_min",
                         "text" => "Shooting in bright light",
                         "good" => "Takes good pictures in bright light",
                         "bad" => "Picture quality in bright light could be better"},
    "iso_max" => {"feature_code" => "iso_max",
                         "text" => "Shooting in low light",
                         "good" => "Takes good pictures in low light",
                         "bad" => "Picture quality in low light could be better"},
    "image_stabilization" => {"feature_code" => "image_stabilization",
                         "text" => "Image Stabilization",
                         "good" => "Prevents shaky images",
                         "bad" => "Could shoot shaky images"},
    "live_view" => {"feature_code" => "live_view",
                    "text" => "Live Preview",
                    "good" => "Shows live preview on screen LCD",
                    "bad" => "Doesn't show live preview"},
    "macro_focus_range_cm" => {"feature_code" => "macro_focus_range_cm",
                    "text" => "Ability to focus close objects",
                    "good" => "Focuses objects close by well",
                    "bad" => "Focusing close by objects could be better"},
    "manual_exposure_mode" => {"feature_code" => "manual_exposure_mode",
                    "text" => "Manual operational mode",
                    "good" => "Supports manual operational mode",
                    "bad" => "Lacks manual operational mode"},
    "manual_focus" => {"feature_code" => "manual_focus",
                    "text" => "Manual focus",
                    "good" => "Supports manual focus",
                    "bad" => "Lacks manual focus"},
    #"maximum_aperture" => 0.9, #TODO
    "max_aperture_zoomout" => {"feature_code" => "max_aperture_zoomout",
                    "text" => "Maximum aperture on zoomout",
                    "good" => "Higher zoomout aperture shoots better images",
                    "bad" => "Zoomout aperture could have been higher"},
    "max_aperture_zoomin" => {"feature_code" => "max_aperture_zoomin",
                    "text" => "Maximum aperture on zoomin",
                    "good" => "Higher zoomin aperture shoots better images",
                    "bad" => "Zoomin aperture could have been higher"},
    "maximum_shutter_speed" => {"feature_code" => "maximum_shutter_speed",
                    "text" => "Ability to avoid blur",
                    "good" => "Freezes pictures and avoids blur",
                    "bad" => "Ability to avoid blurry pictures could be better"},
    "minimum_shutter_speed" => {"feature_code" => "minimum_shutter_speed",
                    "text" => "Ability to take pictures in the dark",
                    "good" => "Takes good pictures in the dark",
                    "bad" => "Ability to take pictures in the dark could be better"},
    "number_of_focus_points" => {"feature_code" => "number_of_focus_points",
                    "text" => "Focusing ability",
                    "good" => "Good Focusing ability",
                    "bad" => "Focusing ability could be better"},
    "optical_zoom" => {"feature_code" => "optical_zoom",
                    "text" => "Ability to photograph far away objects",
                    "good" => "Photographs far away objects well",
                    "bad" => "Could do better in capturing far away objects"},
    "full_hd_fps" => {"feature_code" => "full_hd_fps",
              "text" => "Number of continuous shots the camera can take with full HD",
              "good" => "Higher number of shots in continuous/burst mode with full HD",
              "bad" => "Number of shots in continuous/burst mode with full HDmode could be better"},
    "screen_size" => {"feature_code" => "screen_size",
              "text" => "LCD screen size",
              "good" => "Bigger LCD screen",
              "bad" => "LCD screen could be bigger"},
    "sensor_size_w_mm" => {"feature_code" => "sensor_size_w_mm",
              "text" => "Sensor Width",
              "good" => "Higher sensor width shoots better images",
              "bad" => "Sensor width could be bigger"},
    "sensor_size_h_mm" => {"feature_code" => "sensor_size_h_mm",
              "text" => "Sensor Height",
              "good" => "Larger sensor height shoots better images",
              "bad" => "Sensor height could be bigger"},
    "shutter_priority" => {"feature_code" => "shutter_priority",
                         "text" => "Aperture and Shutter Priority",
                         "good" => "Allows semi automatic shooting modes",
                         "bad" => "Does not allow semi automatic shooting modes"},
    "timelapse_recording" => {"feature_code" => "timelapse_recording",
                         "text" => "Timelapse Recording",
                         "good" => "Allows Timelapse Recording",
                         "bad" => "Does not allow Timelapse Recording"}
  }

  @@features_by_category = {
    "Sensor" => ["max_resolution", "other_resolutions", "image_ratio_w_h", "effective_megapixels", "sensor_size", "sensor_type"],
    "Image" => ["iso", "image_stabilization"],
    "Optics & Focus" => ["autofocus", "digital_zoom", "manual_focus", "number_of_focus_points", "lens_mount", "focal_length_multiplier"],
    "Screen & Viewfinder" => ["articulated_lcd", "screen_size", "screen_dots", "touch_screen", "screen_type", "live_view", "viewfinder_type"],
    "Photography Features" => ["minimum_shutter_speed", "maximum_shutter_speed", "aperture_priority", "shutter_priority", "manual_exposure_mode", "subject_scene_modes", "built_in_flash", "flash_range_mts", "external_flash", "flash_modes", "continuous_drive", "self_timer", "metering_modes", "exposure_compensation", "ae_bracketing", "wb_bracketing"],
    "Videography Features" => ["video_resolutions", "format", "microphone", "speaker"],
    "Storage" => ["storage_types", "storage_included"],
    "Connectivity" => ["usb", "hdmi", "remote_control"],
    "Physical Features" => ["environmentally_sealed", "battery", "battery_description", "battery_life_cipa", "weight_inc_batteries_gms", "dimensions"]
  }
  
  @@other_features = Camera.first.attributes.keys - ["image_urls","default_image_file_name","default_image_content_type","default_image_file_size","default_image_updated_at","sample_images","timeline_info","alternate_urls_and_prices","rating", "id", "price_range", "amzn_url", "amzn_img_url", "amzn_price", "executive_summary"] -  @@features_by_category.values.flatten

  def float_attributes
    @float_attributes ||= self.attributes.select{|k,v| Camera.first.column_for_attribute(k).type.to_s == "float"}.keys
  end

  @@boolean_features = [
  	"ae_bracketing",
  	"aperture_priority",
  	"articulated_lcd",
  	"environmentally_sealed",
  	"external_flash",
  	"gps",
	"hdmi",
	"image_stabilization",
	"live_view",
	"manual_exposure_mode",
	"manual_focus",
	"shutter_priority",
	"timelapse_recording"
  ]

  @@inverse_features = [
  	"focal_length_min",
  	"iso_min"
  ]

  @@common_key_features = [
    ["body_type","Body Type "],
    ["available_modes", "Modes available "],
    ["effective_megapixels", "Effective Megapixels "],
    ["sensor_size", "Sensor Size "],
    ["video", "Video Options "],
    ["environmentally_sealed", "Weatherproofing "],
    ["iso", "ISO "],
    ["lcd_screen_info", "Screen description "]
  ]

  @@slr_key_features = [
    ["continuous_drive", "Burst Mode "]#NA for non SLRs
  ]

  @@non_slr_key_features = [
    ["focal_length", "Zoom Range "] #NA for SLRs
  ]

  @@ignore_for_dslr = [
    "focal_length_min",
    "focal_length_max",
    "macro_focus_range_cm",
    "manual_focus",
    "max_aperture_zoomin",
    "max_aperture_zoomout",
    "optical_zoom"
  ]

  def self.features_by_category
    @@features_by_category
  end

  def self.other_features
    @@other_features
  end

  def self.inverse_features
    @@inverse_features
  end

  def self.feature_descriptions
    @@feature_descriptions
  end

  def set_image_from_url(url)
    puts "saving #{url}"
    begin
      self.default_image = open(url)
      self.save
    rescue => e
      Rails.logger.debug "Error storing image for #{self.id} : #{e.message}"
    end
  end

  def default_image_url
    self.default_image.url :thumb
  end

  def clearlee_rating within_body_type_only=false
    ratings = (within_body_type_only == true) ? clearlee_rating_across_body_type : clearlee_rating_in_price_bracket
  end

  def clearlee_rating_in_price_bracket
    puts " in pb #{self.id}"
    @rating_pb ||= begin
      ratings = Camera.get_rating_points(self.get_cameras_in_price_bracket)
      return 0.0 if ratings.nil? || ratings[self.id].nil?
      rating = ((ratings[self.id].to_f/ratings.values.max.to_f)*100.0).round(2)
      if self.engadget_score.present?
        rating = (rating + self.engadget_score.to_f)/2
      end
      rating
    end
    return @rating_pb
  end

  def clearlee_rating_across_body_type
    puts " in bt #{self.id}"
    @rating_bt ||= begin
      ratings  = Camera.get_rating_points(self.get_cameras_in_body_type)
      return 0.0 if ratings.nil? || ratings[self.id].nil?
      rating = ((ratings[self.id].to_f/ratings.values.max.to_f)*100.0).round(2)
    end
    return @rating_bt
  end


  def self.contains_dslr cameras
    !(cameras.map(&:group).uniq & ["Rangefinder", "DSLR style mirrorless", "Crop Sensor DSLR", "Full frame DSLR"]).empty?
  end


  def self.ignore_for_dslr
    @@ignore_for_dslr
  end

  def full_name
    return model_name if brand.nil? 
    if model_name.starts_with? brand
      model_name
    else
      brand + " " + model_name
    end
  end

  # Compute the rating for the cameras.
  def self.get_rating_points cameras
raise "hell" if !cameras.present? #cameras.map(&:id).inspect
    #contains_dslr_cameras = contains_dslr(cameras)
    feature_weights_set = @@feature_weights
    #feature_weights_set =  feature_weights_set.reject{|k,v| self.ignore_for_dslr.include?(k)}  if contains_dslr_cameras
  	ratings = {}
    bf = @@boolean_features
    #bf = @@boolean_features - ignore_for_dslr  if contains_dslr_cameras
    inverse_features = @@inverse_features
    #inverse_features = @@inverse_features - ignore_for_dslr if contains_dslr_cameras 
  	feature_weights_set.except(*(bf.concat(inverse_features))).except("announced_date").each do |f, w|
      max_val = cameras.map{|c| c.send(f)}.reject{|v| self.not_present_or_applicable?(v)}.map(&:to_f).max
      self.instance_variable_set("@max_#{f}", max_val) 
  	end

		@max_articulated_lcd = cameras.map{|c| c.articulated_lcd}.reject{|v| self.not_present_or_applicable?(v) || v.downcase == "fixed"}.present? ? 1.0: 0.0
  	@max_announced_date = cameras.map{|c| c.announced_date}.reject{|v| self.not_present_or_applicable?(v)}.map{|d| d.strftime("%Y%m%d").to_i}.max
  	@max_number_of_focus_points = cameras.map{|c| c.number_of_focus_points}.map(&:to_i).max
  	@max_battery_life_cipa = cameras.map{|c| c.battery_life_cipa}.map(&:to_i).max

  	#Inverse values, smaller the better
		@min_focal_length_min = cameras.map{|c| (c.focal_length_min.nil? || c.focal_length_min == 0) ? 0 : 1/c.focal_length_min}.min
		@min_iso_min = cameras.map{|c| (c.iso_min == 0 || c.iso_min.nil?) ? 0 : 1/c.iso_min}.min
	  	
  	self.init_boolean_features cameras

  	cameras.each do |c|
  		rating = 0.0
  		bf.uniq.each do |k|
  			val = c.send(k)
  			rating += @@feature_weights[k] if !self.not_present_or_applicable?(val)
  		end

  		self.features_with_no_special_handling.each do |k|
  			val = c.send(k)
  			next if val.nil?
        val = val.to_f
  			rating += (feature_weights_set[k]*val)/self.instance_variable_get("@max_#{k}").to_f if self.instance_variable_get("@max_#{k}") != 0 && self.instance_variable_get("@max_#{k}").present?
  		end

  		inverse_features.each do |k|
  			val = c.send(k)
        val = val.to_f
  			next if val.nil?
  			rating += (feature_weights_set[k]*val)/self.instance_variable_get("@min_#{k}").to_f if self.instance_variable_get("@min_#{k}") != 0
  		end

      if @max_announced_date && c.announced_date
        rating += (feature_weights_set["announced_date"]*c.announced_date.strftime("%Y%m%d").to_i)/@max_announced_date
      end
  		ratings[c.id] = rating
  	end

	  ratings.sort {|a,b| b[1]<=>a[1]}
    Hash[*ratings.flatten]
  end

  def get_good_and_bad_in_price_bracket within_body_type_only=false, input_cameras=[]
    feature_ratings = {}
    return feature_ratings if self.amzn_price.nil?
    cameras = []

    if input_cameras.present?
      cameras = input_cameras
    else
      cameras = within_body_type_only ? self.get_cameras_in_body_type : self.get_cameras_in_price_bracket
    end

    @@boolean_features.each do |f|
      my_rating = self.class.not_present_or_applicable?(self.send(f)) ? 0 : 1
      other_cameras_data = cameras.reject{|c| c.id == self.id}.map{|c| self.class.not_present_or_applicable?(c.send(f)) ? 0 : 1}
      if my_rating == 0
        feature_ratings[f] = {"equal" => other_cameras_data.select{|d| d == 0}.count + 1, 
                              "better" => other_cameras_data.select{|d| d == 1}.count,
                              "worse" => 0,
                              "total" => other_cameras_data.count + 1}
      else
        feature_ratings[f] = {"equal" => other_cameras_data.select{|d| d == 1}.count + 1, 
                              "better" => 0,
                              "worse" => other_cameras_data.select{|d| d == 0}.count,
                              "total" => other_cameras_data.count + 1}
      end
    end

    if self.announced_date.present?
      my_announced_date = self.announced_date
      cameras_with_announced_date = cameras.select{|c| c.announced_date.present? && c.id != self.id}
      feature_ratings["announced_date"] = {
        "better" => cameras_with_announced_date.select{|c| c.announced_date > self.announced_date}.count,
        "worse" => cameras_with_announced_date.select{|c| c.announced_date < self.announced_date}.count,
        "equal" => cameras_with_announced_date.select{|c| c.announced_date == self.announced_date}.count,
        "total" => cameras_with_announced_date.count + 1
      }
    end

    self.class.features_with_no_special_handling.each do |f|
      val = self.send(f)
      next if val.nil?
      val = val.to_f
      set_for_comparison = cameras.select{|c| c.send(f).present? && c.id != self.id}
      feature_ratings[f] = {
        "better" => set_for_comparison.select{|c| c.send(f).to_f > val}.count,
        "worse" => set_for_comparison.select{|c| c.send(f).to_f < val}.count,
        "equal" => set_for_comparison.select{|c| c.send(f).to_f == val}.count,
        "total" => set_for_comparison.count + 1
      }
    end

    @@inverse_features.each do |f|
      val = self.send(f)
      next if val.nil?
      val = val.to_f
      set_for_comparison = cameras.select{|c| c.send(f).present? && c.id != self.id}
      
      feature_ratings[f] = {
        "better" => set_for_comparison.select{|c| c.send(f).to_f < val}.count,
        "worse" => set_for_comparison.select{|c| c.send(f).to_f > val}.count,
        "equal" => set_for_comparison.select{|c| c.send(f).to_f == val}.count,
        "total" => set_for_comparison.count + 1
      }
    end
    feature_ratings_desc = {}
    feature_ratings.each{|k,v| feature_ratings_desc[@@feature_descriptions[k]] = v}
    return feature_ratings_desc
  end

  def self.get_features_for_price_jump price1, price2
    cameras_in_range_1 = Camera.where('amzn_price > ? and amzn_price < ?', price1 * 0.9, price1 * 1.1).order("amzn_price")
    cameras_in_range_2 = Camera.where('amzn_price > ? and amzn_price < ?', price2 * 0.9, price2 * 1.1).order("amzn_price")

    c1 = Camera.new
    c2 = Camera.new

    self.features_with_no_special_handling.each do |k|
      next if k == "minimum_shutter_speed"
      values = cameras_in_range_1.map{|c| c.send(k)}.map!{|x| x.nil? ? 0 : x.to_f}
      c1.send("#{k}=",values.instance_eval { reduce(:+) / size.to_f })
      values = cameras_in_range_2.map{|c| c.send(k)}.map!{|x| x.nil? ? 0 : x.to_f}
      c2.send("#{k}=",values.instance_eval { reduce(:+) / size.to_f })
    end

    self.inverse_features.each do |k|
      values = cameras_in_range_1.map{|c| c.send(k)}.map!{|x| x.nil? ? 0 : x.to_f}
      c1.send("#{k}=",values.instance_eval { reduce(:+) / size.to_f })
      values = cameras_in_range_2.map{|c| c.send(k)}.map!{|x| x.nil? ? 0 : x.to_f}
      c2.send("#{k}=",values.instance_eval { reduce(:+) / size.to_f })
    end

    @@boolean_features.each do |bf|
      values = cameras_in_range_1.map{|c| c.send(bf)}
      if((values.select{|v| !Camera.not_present_or_applicable?(v)}.count)/(values.count.to_f) < 0.5)
        c1.send("#{bf}=", 0)
      else
        c1.send("#{bf}=", 1)
      end

      values = cameras_in_range_2.map{|c| c.send(bf)}
      if((values.select{|v| !Camera.not_present_or_applicable?(v)}.count)/(values.count.to_f) < 0.5)
        c2.send("#{bf}=", 0)
      else
        c2.send("#{bf}=", 1)
      end
    end


    feature_comparison = c1.compare c2
    what_is_extra = {}
    h = {}
    feature_comparison["differences"].each{|d| h[d] = [c1.send(d), c2.send(d)]}

    h.reject{|k,v| v[0].nil? && v[1].nil?}.each do |k,v|
      if !Camera.inverse_features.include?(k)
        what_is_extra[k] = [v[0], v[1]] if((v[0].present? && v[1].present? && (v[0] < v[1])) || (v[0].nil? && v[1].present?)) 
      else
        what_is_extra[k] = [v[0], v[1]] if((v[0].present? && v[1].present? && (v[0] > v[1])) || (v[1].nil? && v[0].present?)) 
      end
    end
    what_is_extra
  end

  # camera fearutes that have boolean values
  def self.init_boolean_features cameras
  	@@boolean_features.each do |f|
  		self.instance_variable_set("@max_#{f}", 
  			((cameras.map{|c| c.send(f)}.reject{|v| self.not_present_or_applicable?(v)}.present?) ? 1.0: 0.0)
  			)
  	end
  end

  def self.not_present_or_applicable? val
  	val == '' || val == nil || 
  	(val.is_a?(String) && (val.downcase == 'unknown' || val.downcase == 'no' || val.downcase == 'none'))
  end

  def self.features_with_no_special_handling
    @@feature_weights.keys - @@boolean_features - @@inverse_features - ['announced_date']
  end

  def compare(other, important_features_only = true)
    similarities = []
    differences = []
    fields = important_features_only ? @@feature_weights.keys : self.class.column_names
    fields.each do |c|
      if self.send(c) == other.send(c)
        similarities << c
      else
        differences << c
      end
    end
    return {"similarities" => similarities, "differences" => differences}
  end  

  def key_features
    key_feature_set = @@common_key_features 
    key_feature_set += self.slr? ? @@slr_key_features : @@non_slr_key_features
    @key_features ||= begin   
      key_features = {}   
      key_feature_set.each {|f| key_features[f[1]] = self.send f[0] }
      key_features.reject{|k,v| v == nil || v == "No"}
    end
  end

  def available_modes
    @modes ||= begin
      _modes = ["Fully Automatic"]
      _modes << "Semi Automatic" if self.shutter_priority == "Yes" && self.aperture_priority == "Yes"
      _modes << "Fully manual" if self.manual_exposure_mode == "Yes"
      _modes.join ", "
    end
  end

  def sensor_size
    @sensor_size ||= (self.sensor_size_w_mm.to_s + " x " + self.sensor_size_h_mm.to_s + " mm")
  end

  def slr?
    @slr ||= self.body_type.match("SLR").present?
  end

  def focal_length
    return nil if self.focal_length_min.nil? && self.focal_length_max.nil?
    @focal_length ||= begin
      f_max = self.focal_length_max.nil? ? self.focal_length_min : self.focal_length_max
      focal_length = self.focal_length_min.to_s + " - " + f_max.to_s + " mm"
    rescue Exception => e
      puts e.message
    end
  end

  def video
    return nil if self.video_resolutions.nil?
    @video ||= begin
      video = self.video_resolutions.match(/1920 x 1080/).present? ? "Shoots true HD" : "Shoots video"
    end
  end

  def lcd_screen_info
    #return nil if self.articulated_lcd.nil? && self.screen_size.nil?
    @lcd_screen_info ||= begin
      screen_info = self.articulated_lcd + " " if self.articulated_lcd.present?
      screen_info += self.screen_size.to_s + " inches" if self.screen_size.present?

    rescue Exception => e
    end
  end

  def get_sample_images from_flickr = false
    @sample_images ||= begin
      imgs = (from_flickr == true) ? FlickrInterface.sample_images(self.model_name) :self.sample_images
      sample_imgs = []
      imgs.each do |i|
        sample_imgs << i.grep(/_c.jpg|_b.jpg/)
      end
      sample_imgs.uniq
    end  
  end

   def similar_products limit_to=6
     @similar_products ||= begin
      if(amzn_price.nil?)
        {:similar_products => [], :other_products_by_brand => []}
      else  
        similar_products = Camera.with_price.where({amzn_price: self.amzn_price*0.9..self.amzn_price*1.1, group: self.group}).sort{|a,b| (a.amzn_price - self.amzn_price).abs <=> (b.amzn_price - self.amzn_price).abs}.to_a[0..limit_to-1]
        Camera.where({group: self.group}).where('amzn_price is not null && amzn_price != ""').order("amzn_price").limit(limit_to).to_a if similar_products.count == 0
        other_products_by_brand = Camera.with_price.where({brand: self.brand, group: self.group}).limit(limit_to).order("amzn_price").to_a
        {:similar_products => similar_products - [self], :other_products_by_brand => other_products_by_brand - [self]}
      end
     end
   end

  def self.feature_weights
    @@feature_weights
  end

  def additional_images_for_display
    if self.image_urls
      self.image_urls.select{|k,v| k.to_s.match(/(^large|^back|^angle)/) && v.present?}.try(:values)
    else
      []
    end
  end

  def get_dimension name
    return "" if self.dimensions.nil? || self.dimensions == ""
    dims = self.dimensions.split(",")
    return "" if dims.empty?
    var = name.capitalize
    h = dims.split(",").flatten.grep(/#{var}/).try(:first)
    return h.present? ? (h.split(":").last.to_f * 0.0393701).round(2) : ""
  end

  def self.groups
    @groups ||= Camera.all.map(&:group).reject(&:nil?).uniq
  end
 
  def cameras_in_group group
    Camera.where('group = ?', group)
  end
  
  def self.top_cameras number, criterion, value
    puts "#{value}"
    list = []
    if criterion == "price"
      list = Camera.where({amzn_price: value.to_f*0.9..value.to_f*1.1})
    elsif criterion == "type"
      list = Camera.where({group: value}).order('amzn_price desc')  #.limit(200)
    else
      return []
    end
    puts "Processing #{criterion}: #{list.size} : #{value}"
    [list.count, Camera.prune_cameras_by_rating(list, number, criterion)]
  end

  def self.prune_cameras_by_rating cameras, number, criterion
    ratings = {}
    puts "processing #{cameras.count}"
    if criterion == "type"
      cameras.reject{|c| c.amzn_price == 0.0 || c.amzn_price.nil? }.each{|c| ratings[c.id] = c.clearlee_rating(true)}
    else
      cameras.reject{|c| c.amzn_price == 0.0 || c.amzn_price.nil? }.each{|c| ratings[c.id] = c.clearlee_rating}
    end
    ret = ratings.reject{|k,v| v.nil?}.sort_by{|k,v| v}.reverse[0..number.to_i-1]
    ret
  end

  def minimum_price
    (!self.alternate_urls_and_prices.present? || (self.alternate_urls_and_prices["price"].present? && 
      self.alternate_urls_and_prices["price"] != "" && self.amzn_price.to_f < self.alternate_urls_and_prices["price"].to_f)) ? self.amzn_price : self.alternate_urls_and_prices["price"]
  end

  def verdict clearlee_rating, clearlee_rating_across_body_type
    puts "Rating for #{self.id}"
    if clearlee_rating.to_f > 95.0 && clearlee_rating_across_body_type.to_f > 95.0 && 
      self.announced_date.present? && self.announced_date > 2.years.ago
      return "Clearlee Pick - Best in Class"
    end  
    if clearlee_rating.to_f > 95.0 && clearlee_rating_across_body_type.to_f > 95.0
      return "Great Buy"
    end
    if clearlee_rating.to_f > 92.0  || clearlee_rating_across_body_type.to_f > 92.0
      return "Good Buy"
    end
    return "Not Recommended"
  end

  def verdict_in_group
    if clearlee_rating_across_body_type.to_f > 95.0 && 
      self.announced_date.present? && self.announced_date > 2.years.ago
      return "Clearlee Pick - Best in Class"
    end  
    if clearlee_rating_across_body_type.to_f > 95.0
      return "Great Buy"
    end
    if clearlee_rating_across_body_type.to_f > 92.0
      return "Good Buy"
    end
    return "Not Recommended"    
  end

  def verdict_with_timeline
  end  

  def avg_price group
    Camera.select{|c| c.group == group && c.amzn_price.present?}.map{|c| c[:amzn_price]}.instance_eval{reduce(:+)/size.to_f}
  end

  def get_cameras_in_price_bracket
    price = self.amzn_price == 0.0 || self.amzn_price.nil? ? avg_price(self.group) : self.amzn_price
    cameras = Camera.where('amzn_price > ? and amzn_price < ? and cameras.`group` = ?', price * 0.9, price * 1.1, self.group)
    cameras = get_cameras_in_body_type if cameras.nil? || cameras.size <= 1 

    return cameras
  end

  def get_cameras_in_body_type
    Camera.where('cameras.group = ?', self.group)
  end

end
