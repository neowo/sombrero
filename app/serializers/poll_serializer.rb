
class PollSerializer < ActiveModel::Serializer

  attributes :id, :products, :user_id, :url, :image_urls, :question, :title
  # Get the URL of the request in the response
  def url
    poll_url(object)
  end
end
